<?php
foreach ($tax_rates as $tax) {
    $tr[$tax->id] = $tax->name;
}
?>
<style type="text/css">
@media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px)  {
    /*#dyTable tbody td:nth-of-type(1):before { content: "<?= lang('no'); ?>"; }*/
    #dyTable tbody td:nth-of-type(2):before { content: "<?= lang('quantity'); ?>"; }
    #dyTable tbody td:nth-of-type(3):before { content: "<?= lang('product_code'); ?>"; }
    #dyTable tbody td:nth-of-type(4):before { content: "<?= lang('unit_price'); ?>"; }
    #dyTable tbody td:nth-of-type(5):before { content: "<?= lang('discount'); ?>"; }
    #dyTable tbody td:nth-of-type(6):before { content: "<?= lang('tax_rate'); ?>"; }
    #dyTable tbody td:nth-of-type(7):before { content: "<?= lang('subtotal'); ?>"; }
}
</style>

<div class="row">

    <div class="col-md-4">
        <div class="form-group">
            <label for="date"><?= lang("date"); ?></label>
            <?php $date = date('d/m/Y'); ?>
            <?= form_input('date',$date, 'class="form-control date" id="date"');?>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="billing_company"><?= lang("billing_company"); ?></label>
            <?php
            $bc[""] = lang("select")." ".lang("billing_company");
            foreach ($companies as $company) {
                $bu[$company->id] = $company->company;
            }
            echo form_dropdown('billing_company', $bu, (isset($_POST['billing_company']) ? $_POST['billing_company'] : ($inv ? $inv->company_id : '')), 'class="billing_company form-control" data-placeholder="'.lang("select")." ".lang("billing_company").'" id="billing_company"');
            ?>
        </div>
    </div>
    
    <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("PO No"); ?></label>
            <div class="input-group">
                < ?= form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ($inv ? $inv->reference_no : '')), 'class="form-control" id="reference_no"'); ?>
                <span class="input-group-addon" id="gen_ref" style="cursor: pointer;"><i class="fa fa-random"></i></span>
                <div class="clearfix"></div>
            </div>
        </div>
    </div> -->
    <div class="col-md-4">
        <div class="form-group">
            <label for="status"><?= lang("status"); ?></label>
            <?php
            if (!$q) {
                $st = array(
                    ''      => lang("select")." ".lang("status"),
                    'canceled' => lang('canceled'),
                    'overdue'   => lang('overdue'),
                    'paid'      => lang('paid'),
                    'pending'   => lang('pending')
                );
            } else {
                $st = array(
                    ''      => lang("select")." ".lang("status"),
                    'canceled' => lang('canceled'),
                    'ordered'   => lang('ordered'),
                    'pending'   => lang('pending'),
                    'sent'  => lang('sent')
                );
            }
            echo form_dropdown('status', $st, (isset($_POST['status']) ? $_POST['status'] : ($inv ? $inv->status : '')), 'class="status form-control" data-placeholder="'.lang("select")." ".lang("status").'" id="status"');
            ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="customer"><?= lang("Project"); ?><span style="color:red"> *</span></label>
           
            <select class="form-control" name="project" id="project"  required>
                <option value="">Select</option>
                <?php if(!empty($project)){
            foreach ($project as $projects) { ?>
                <option value="<?= $projects->id?>" data-customer="<?=$projects->customer_id?>" data-retention="<?=$projects->retention_amount ?>" data-percentage="<?=$projects->adv_payment_percentage?>" data-apa="<?=$projects->adavnce_payment_per_amount?>" data-contractual="<?= $projects->price?>" ><?= $projects->code?> - <?= $projects->name?></option>
                <?php } }?>
            </select>
                
            
           
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="customer"><?= lang("customer"); ?></label>
            <select class="form-control" name="customer" id="customer" >
                <option value="">Select</option>
                <?php if(!empty($customers)){
            foreach ($customers as $customerss) { ?>
                <option value="<?= $customerss->id?>" ><?= $customerss->company?></option>
                <?php } }?>
            </select>
        </div>
    </div>
    
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Subject"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"  class="form-control" name="subject" id="subject"> 
          
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Subject In Other Language"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"  class="form-control" name="subject_other" id="subject_other"> 
          
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
    <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Progressive Payment Amount Received"); ?></label>
            <input type="text"  class="form-control" name="progressive_payment_amount" id="progressive_payment_amount"> 
           
            <div class="clearfix"></div>
            
        </div>
    </div> -->
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Advance Payment Percentage "); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"  class="form-control" name="adv_pay_per" id="adv_pay_per"> 
            <input type="hidden" readonly class="form-control" name="contractual_value" id="contractual_value">  
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
  <!--  <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Advance Payment Percentage Amount"); ?></label>
           
           
           
            <input type="text"  class="form-control" name="adv_pay_per_amount" id="adv_pay_per_amount">   
            <div class="clearfix"></div>
          
        </div>
    </div> -->
   <!--  <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Retention Amount"); ?></label> 
            <!-- <div class="input-group"> -->
          <!--  <input type="text"  class="form-control" name="retention_amt" id="retention_amt">  
          
        </div>
    </div> -->
     <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Gross Value"); ?></label>
            <div class="input-group"> -->
           <!-- <input type="hidden" readonly  class="form-control" name="gross_value" id="gross_value">   
            <!-- <div class="clearfix"></div> -->
            <!-- </div> -->
        <!-- </div> -->
    <!-- </div> -->
    <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Work Done Percentage"); ?></label>
             <input type="text"   class="form-control" name="wdp" id="wdp">   
           
            <div class="clearfix"></div>
           </div>
    </div> -->
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Gross Payment"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"   class="form-control" name="gross_payment" id="gross_payment">   
            <input type="hidden" name="tax_rate" id="tax_rate">
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
    
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Percentage of work done"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"   class="form-control" name="percentage_work_done" id="percentage_work_done">   
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="retantion_percentage"><?= lang("Retention Percentage"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"   class="form-control" name="retantion_percentage" id="retantion_percentage">   
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
   <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Tax for advance payment"); ?></label>
            <div class="input-group">
            <input type="text" readonly class="form-control" name="tax_adv_payment" id="tax_adv_payment">   
            <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Total amount paid as advance"); ?></label>
            <div class="input-group">
            <input type="text" readonly class="form-control" name="total_amt_paid_adv" id="total_amt_paid_adv">   
            <div class="clearfix"></div>
            </div>
        </div>
    </div>  -->
    
</div>

<script>
    $( document ).ready(function() {
        $("#project").change(function(){
            var tax_rate= <?=$tax_rates[0]->rate?>;
            var contractual = $('#project option:selected').data('contractual');
          //   var adv_pay_per = $('#project option:selected').data('percentage');
          //   var retention_amt = $('#project option:selected').data('retention');
          //   var customer = $('#project option:selected').data('customer');
           //  var apa = $('#project option:selected').data('apa');
           //  var gv =contractual - apa - retention_amt;
           //  $('#gross_value').val(gv);
             $('#contractual_value').val(contractual);
            // $('#adv_pay_per').val(adv_pay_per);
           //  $('#adv_pay_per_amount').val(apa);
           //  $('#retention_amt').val(retention_amt);
           //  $("select#customer").val(customer);
           
             $("#tax_rate").val(tax_rate);
            // var adv_payment=(adv_pay_per*contractual)/100;
            // $("#adv_payment").val(adv_payment);
           //  var tap = (tax_rate*adv_payment)/100;
           //  $("#tax_adv_payment").val(tap);
            // var pp = adv_payment + tap;
           //  $("#total_amt_paid_adv").val(pp);
             $('.cv').val(contractual);
            
           
             

    
});
});
// $(document).on(
//     'change',
//     '.price',
//     function () {
//         var project =$("#project").val();
//         if(project == "")
//         {
//             alert("Please select project");
//             $("#unit").val("");
//             $("#price").val("");
//             $("#description").val("");
//         }else{
//         var tax_rate= <?=$tax_rates[0]->rate?>;
//         var adv_pay_per =$("#adv_pay_per").val();
//         var unit= $("#unit").val();
//         var price= $("#price").val();
//         var total= unit*price;
//        $("#subtotal").val(total);
//        $('.total_Amt').text(number_format(total));
//        var adv_py_amount=(total*adv_pay_per)/100;
      
//        $(".adv_py_amount").text(number_format(adv_py_amount));
//        var tap =(tax_rate*adv_py_amount)/100;
//        $(".tax_adv_py").text(number_format(tap));
//        var pp= adv_py_amount+tap;
//        alert(pp);
//        $('.tot_amt_paid_adv').text(number_format(pp));
//         }
//     }
//   );
    </script>