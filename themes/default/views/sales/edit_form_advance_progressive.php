<?php
foreach ($tax_rates as $tax) {
    $tr[$tax->id] = $tax->name;
}
?>
<style type="text/css">
@media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px)  {
    /*#dyTable tbody td:nth-of-type(1):before { content: "<?= lang('no'); ?>"; }*/
    #dyTable tbody td:nth-of-type(2):before { content: "<?= lang('quantity'); ?>"; }
    #dyTable tbody td:nth-of-type(3):before { content: "<?= lang('product_code'); ?>"; }
    #dyTable tbody td:nth-of-type(4):before { content: "<?= lang('unit_price'); ?>"; }
    #dyTable tbody td:nth-of-type(5):before { content: "<?= lang('discount'); ?>"; }
    #dyTable tbody td:nth-of-type(6):before { content: "<?= lang('tax_rate'); ?>"; }
    #dyTable tbody td:nth-of-type(7):before { content: "<?= lang('subtotal'); ?>"; }
}
</style>

<div class="row">

    <div class="col-md-4">
        <div class="form-group">
            <label for="date"><?= lang("date"); ?></label>
            <?php $date = date('Y-m-d'); ?>
            <?= form_input('date',  date('d/m/Y', strtotime($inv[0]->date)) , 'class="form-control date" id="date"');?>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="billing_company"><?= lang("billing_company"); ?></label>
            <?php
            $bc[""] = lang("select")." ".lang("billing_company");
            foreach ($companies as $company) {
                $bu[$company->id] = $company->company;
            }
            echo form_dropdown('billing_company', $bu, (isset($_POST['billing_company']) ? $_POST['billing_company'] : ($inv ? $inv[0]->billing_company_id : '')), 'class="billing_company form-control" data-placeholder="'.lang("select")." ".lang("billing_company").'" id="billing_company"');
            ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="status"><?= lang("status"); ?></label>
            <?php
            if (!$q) {
                $st = array(
                    ''      => lang("select")." ".lang("status"),
                    'canceled' => lang('canceled'),
                    'overdue'   => lang('overdue'),
                    'paid'      => lang('paid'),
                    'pending'   => lang('pending')
                );
            } else {
                $st = array(
                    ''      => lang("select")." ".lang("status"),
                    'canceled' => lang('canceled'),
                    'ordered'   => lang('ordered'),
                    'pending'   => lang('pending'),
                    'sent'  => lang('sent')
                );
            }
            echo form_dropdown('status', $st, (isset($_POST['status']) ? $_POST['status'] : ($inv ? $inv[0]->status : '')), 'class="status form-control" data-placeholder="'.lang("select")." ".lang("status").'" id="status"');
            ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="customer"><?= lang("Project"); ?><span style="color:red"> *</span></label>
           
            <select class="form-control" name="project" id="project"  required>
                <option value="">Select</option>
                <?php if(!empty($project)){
            foreach ($project as $projects) { ?>
                <option <?php if($projects->id == $inv[0]->product_id){ echo "selected";} ?> value="<?= $projects->id?>" data-customer="<?=$projects->customer_id?>" data-retention="<?=$projects->retention_amount ?>" data-percentage="<?=$projects->adv_payment_percentage?>" data-apa="<?=$projects->adavnce_payment_per_amount?>" data-contractual="<?= $projects->price?>" ><?= $projects->name?></option>
                <?php } }?>
            </select>
                
            
           
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="customer"><?= lang("customer"); ?></label>
            <select class="form-control" name="customer" id="customer" >
                <option value="">Select</option>
                <?php if(!empty($customers)){
            foreach ($customers as $customerss) { ?>
                <option value="<?= $customerss->id?>" <?php if($customerss->id == $inv[0]->customer_id){ echo "selected";} ?> ><?= $customerss->company?></option>
                <?php } }?>
            </select>
        </div>
    </div>
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Subject"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text" value="<?=(isset($_POST['subject']) ? $_POST['subject'] : ($inv ? $inv[0]->subject : ''))?>"  class="form-control" name="subject" id="subject"> 
          
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Subject In Other Language"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text" value="<?=($inv ? $inv[0]->subject_other : '')?>" class="form-control" name="subject_other" id="subject_other"> 
          
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
    <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Progressive Payment Amount Received"); ?></label>
            <input type="text" value="<?=$inv[0]->progressive_payment_amt_rec?>"  class="form-control" name="progressive_payment_amount" id="progressive_payment_amount"> 
           
            <div class="clearfix"></div>
         
        </div>
    </div> -->
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Advance Payment Percentage "); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"  value="<?=$inv[0]->advance_payment_percentag ?>" class="form-control" name="adv_pay_per" id="adv_pay_per"> 
            <input type="hidden" value="<?=$inv[0]->contractual_value ?>" readonly class="form-control" name="contractual_value" id="contractual_value">  
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
  <!--  <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Advance Payment Percentage Amount"); ?></label>
           
           
           
            <input type="text" value="<?=(isset($_POST['adv_pay_per_amount']) ? $_POST['adv_pay_per_amount'] : ($inv ? $inv[0]->adavnce_payment_per_amount : ''))?>" class="form-control" name="adv_pay_per_amount" id="adv_pay_per_amount">   
            <div class="clearfix"></div>
         
        </div>
    </div> -->
    <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Retention Amount"); ?></label> 
            <input type="text" value="<?=(isset($_POST['retention_amt']) ? $_POST['retention_amt'] : ($inv ? $inv[0]->retention_amt : ''))?>" class="form-control" name="retention_amt" id="retention_amt">  
            
        </div>
    </div> -->
    
    <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Contractual Value"); ?></label>
       
            <input type="text" readonly class="form-control" name="contractual_value" id="contractual_value">  
           
          
            </div>
        </div>
    </div>   -->
     <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Advance Payment Percentage Amount"); ?></label>
            <input type="hidden" readonly class="form-control" name="adv_pay_per" id="adv_pay_per"> 
            <input type="hidden" readonly class="form-control" name="retention_amt" id="retention_amt">  
           
            <input type="text" readonly class="form-control" name="adv_pay_per_amount" id="adv_pay_per_amount">   
            <div class="clearfix"></div>
         
        </div>
    </div> -->
    <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Retention Amount"); ?></label> -->
            <!-- <div class="input-group"> -->
            
            <!-- <div class="clearfix"></div> -->
            <!-- </div> -->
        <!-- </div>
    </div> -->
     <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Gross Value"); ?></label>
            <div class="input-group"> -->
            <input type="hidden" readonly  class="form-control" name="gross_value" id="gross_value">   
            <!-- <div class="clearfix"></div> -->
            <!-- </div> -->
        <!-- </div> -->
    <!-- </div> -->
    <!-- <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Work Done Percentage"); ?></label>
             <input type="text"   class="form-control" name="wdp" id="wdp">   
           
            <div class="clearfix"></div>
           </div>
    </div> -->
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Gross Payment"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"   class="form-control" value="<?=(isset($_POST['gross_payment']) ? $_POST['gross_payment'] : ($inv ? $inv[0]->gross_payment : ''))?>" name="gross_payment" id="gross_payment">   
            <input type="hidden" name="tax_rate" id="tax_rate" value="<?=(isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($inv ? $inv[0]->tax_rate : ''))?>">
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
    
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Percentage of work done"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"   class="form-control"  value="<?=(isset($_POST['percentage_work_done']) ? $_POST['percentage_work_done'] : ($inv ? $inv[0]->percentage_work_done : ''))?>" name="percentage_work_done" id="percentage_work_done">   
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="retantion_percentage"><?= lang("Retention Percentage"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"   class="form-control"value="<?=(isset($_POST['retantion_percentage']) ? $_POST['retantion_percentage'] : ($inv ? $inv[0]->retantion_percentage : ''))?>" name="retantion_percentage" id="retantion_percentage">   
            <div class="clearfix"></div>
            <!-- </div> -->
        </div>
    </div>
   
    
</div>


<script>
    $( document ).ready(function() {
       // $("#project").change(function(){
            // var tax_rate= <?=$tax_rates[0]->rate?>;
            //  var contractual = $('#project option:selected').data('contractual');
            //  var adv_pay_per = $('#project option:selected').data('percentage');
            //  var retention_amt = $('#project option:selected').data('retention');
            //  var customer = $('#project option:selected').data('customer');
            //  var apa = $('#project option:selected').data('apa');
            //  var gv =contractual - apa - retention_amt;
            //  $('#gross_value').val(gv);
            //  $('#contractual_value').val(contractual);
            //  $('#adv_pay_per').val(adv_pay_per);
            //  $('#adv_pay_per_amount').val(apa);
            //  $('#retention_amt').val(retention_amt);
            //  $("select#customer").val(customer);
           
            //  $("#tax_rate").val(tax_rate);
            // var adv_payment=(adv_pay_per*contractual)/100;
            // $("#adv_payment").val(adv_payment);
           //  var tap = (tax_rate*adv_payment)/100;
           //  $("#tax_adv_payment").val(tap);
            // var pp = adv_payment + tap;
           //  $("#total_amt_paid_adv").val(pp);
            // $('.cv').val(contractual);
            
           
             

    
//});
});
$( document ).ready(function() {
        $("#project").change(function(){
            var tax_rate= <?=$tax_rates[0]->rate?>;
             var contractual = $('#project option:selected').data('contractual');
             var adv_pay_per = $('#project option:selected').data('percentage');
             var retention_amt = $('#project option:selected').data('retention');
             var customer = $('#project option:selected').data('customer');
             var apa = $('#project option:selected').data('apa');
             var gv =contractual - apa - retention_amt;
             $('#gross_value').val(gv);
             $('#contractual_value').val(contractual);
             $('#adv_pay_per').val(adv_pay_per);
             $('#adv_pay_per_amount').val(apa);
             $('#retention_amt').val(retention_amt);
             $("select#customer").val(customer);
           
             $("#tax_rate").val(tax_rate);
            // var adv_payment=(adv_pay_per*contractual)/100;
            // $("#adv_payment").val(adv_payment);
           //  var tap = (tax_rate*adv_payment)/100;
           //  $("#tax_adv_payment").val(tap);
            // var pp = adv_payment + tap;
           //  $("#total_amt_paid_adv").val(pp);
             $('.cv').val(contractual);
            
           
             

    
});
});

    </script>