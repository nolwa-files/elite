    <div class="container">
<div class="row" style="margin-top: 70px;">
                <div class="col-xs-6 ">
                        <b><h3 style="font-family: Tahoma, Verdana, Segoe, sans-serif;">TAX INVOICE <br> فاتورة ضريبية</h3><b>
                </div>
                <div class="col-xs-6 " style="text-align: right;" >
					 <?php 
                   $inv_date = date("Y-m-d h:i:s", strtotime($inv[0]->date));
                   $vat_total = $inv[0]->new_vat;
                   $grand_total = $inv[0]->total_amount;
                   $seller = $biller->company;
                   $Vat_No = $biller->cf1;
                   $result = chr(1) . chr( strlen($seller) ) . $seller;
                   $result.= chr(2) . chr( strlen($Vat_No) ) . $Vat_No;
                   $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                   $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                   $result.= chr(5) . chr( strlen($vat_total) ) . $vat_total;
                   $newQr = base64_encode($result); 
                  //echo "<pre>";print_r($result);die;
                   ?>
                <?= $this->sim->qrcode('url', $newQr, 0); ?>
				</div>
                 <div class="col-xs-12 padding010">
                 <!-- <div class="clearfix"></div> -->
                    <div class="table-responsive">
                        <table class="table  print-table order-table">
                        <tr>
                             <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Invoice Date:</td>
                             <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=  date("d/m/Y", strtotime($inv[0]->date)); ?></td>
                             <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-right"><?=  date("d/m/Y", strtotime($inv[0]->date)); ?></td>
                             <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">تاريخ:</td>
                         </tr>
                        
                         <tr>
                             <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Project</td>
                             <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=$prooduct[0]->code?>  - <?=$prooduct[0]->name?></td>
                             <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-right"> <?=$prooduct[0]->name_other?> - <?=$prooduct[0]->code?></td>
                             <td style="direction: rtl;ont-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">المشروع    :</td>
                         </tr>
                         <tr>
                                     <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Invoice Number:</td>
                                     <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"> <?= $inv[0]->invoice_id; ?> </td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"class="text-right"> <?= $inv[0]->invoice_id; ?> </td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">رقم الفاتورة:</td>
                         </tr>
                         <tr>
                             <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Work Order Reference No</td>
                             <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"> <?=$prooduct[0]->reference?></td>
                             <td class="text-right" style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"> <?=$prooduct[0]->reference?></td>
                             <td style="direction: rtl;" style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">رقم مرجع أمر العمل :</td>
                         </tr>
                       
                     </table>
                 </div>
                    
                         </div>
                        
                         
                         <div class="col-xs-12 ">
                            <div class="table-responsive">
                                <table class="table  print-table order-table">
                                    <thead>
                                        <tr class="bg-dark">
                                            <th  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-left" colspan="2">Seller:</th>
                                            <th  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-right" colspan="2" style="direction: rtl;">تاجر:</th>
                                            <th  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-left" colspan="2">Buyer:</th>
                                            <th  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-right" colspan="2" style="direction: rtl;">مشتر:</th>
                                        </tr>
                                </thead>
                                <tbody>
                                 <tr >
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Name</td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><b><?= $biller->company && $biller->company != '-' ? $biller->company : $biller->name; ?></b></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><b><?= $biller->company_other && $biller->company_other != '-' ? $biller->company_other : $biller->name_other; ?></b></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">اسم:</td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Name</td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?= $customer->company && $customer->company != '-' ? $customer->company : $customer->name; ?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?= $customer->company_other && $customer->company_other != '-' ? $customer->company_other : $customer->name_other; ?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">اسم:</td>
                                 </tr>
                                 <!-- <tr >
                                     <td style="font-size:10px">Phone </td>
                                     <td style="font-size:10px"><?=$biller->phone?></td>
                                     <td style="direction: rtl;font-size:10px"><?=$biller->phone?></td>
                                     <td style="direction: rtl;font-size:10px">هاتف:</td>
                                     <td style="font-size:10px">phone </td>
                                     <td  style="font-size:10px"><?=$customer->phone?></td>
                                     <td style="direction: rtl;font-size:10px"><?=$customer->phone?></td>
                                     <td style="direction: rtl;font-size:10px">هاتف:</td>
                                 </tr>
                                 <tr >
                                     <td style="font-size:10px">Email </td>
                                     <td style="font-size:10px"><?= $biller->email; ?></td>
                                     <td style="direction: rtl;font-size:10px"><?= $biller->email; ?></td>
                                     <td style="direction: rtl;font-size:10px">البريد الإلكتروني:</td>
                                     <td style="font-size:10px">Email </td>
                                     <td  style="font-size:10px"><?= $customer->email; ?></td>
                                     <td style="direction: rtl;font-size:10px"><?= $customer->email; ?></td>
                                     <td style="direction: rtl;font-size:10px">البريد الإلكتروني:</td>
                                 </tr> -->
                                 <tr >
                                     <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">VAT No</td>
                                     <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" ><?= $biller->cf1?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?= $biller->cf1?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">ظريبه الشراء:</td>
                                     <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">VAT No</td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=$customer->cf1?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=$customer->cf1?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">ظريبه الشراء:</td>
                                 </tr>
                                 <tr >
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Address </td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" ><?= $biller->address?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?= $biller->cf4?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">تبوك :</td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Address </td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" ><?=$customer->address?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=$customer->cf5?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">تبوك :</td>
                                 </tr>
                                 <tr >
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Subject </td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="3" ><?= $inv[0]->subject ?></td>
                                   
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="3" ><?= $inv[0]->subject_other ?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">موضوع</td>
                                 </tr>
                                </tbody>
                             </table>
                           
                                <table class="table  print-table order-table">
                                    

                                    <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2">Total</td><td colspan="2"><?=number_format($inv[0]->contractual_value,2)?></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">مجموع </td></tr>
                                    <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2">Advance Payment Amount</td><td colspan="2"><?= number_format($inv[0]->adavnce_payment_per_amount,2) ?></td><td style="direction: rtl;font-size:10px">مبلغ الدفعة المقدمة  </td></tr>
                                    <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2"><?=$inv[0]->tax_rate?> % Tax for Advance Payment</td><td colspan="2"><?php $tap = ($inv[0]->adavnce_payment_per_amount*$inv[0]->tax_rate)/100; echo number_format($tap,2);?></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=$inv[0]->tax_rate?> ٪ ضريبة على الدفعة المقدمة
</td></tr>
                                    <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2">Total amount paid as advance</td><td colspan="2"><?php echo number_format(($tap + $inv[0]->adavnce_payment_per_amount),2);?></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">االمبلغ الإجمالي المدفوع مقدما  </td></tr>
                                    <tr>
                         
                       
                                        
                                </table>
                               <!--  <p>Best Regards</p>-->
                                <!-- <p><?=$prooduct[0]->regad_name?></p> -->
                                <!-- <p><?=$prooduct[0]->phone?></p> -->
                                <!--  <p><?=$prooduct[0]->designation?></p>-->
                              
                               <span style="font-family: Tahoma, Verdana, Segoe, sans-serif;">IBAN    : <?=$biller->cf3?>   رقم الحساب بصيغة</span> 
    <br>
								 <span >CR NO   
 : <?=$biller->cf2?> رقم السجل التجاري
 </span>
        </div>
</div>
<script>
   $( document ).ready(function() {
   
    window.print();

});

    </script>

        