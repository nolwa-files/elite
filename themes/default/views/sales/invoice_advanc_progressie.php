
<div class="container">
<div class="row" style="margin-top: 70px;">
                <div class="col-xs-6 ">
                        <b><h3 style="font-family: Tahoma, Verdana, Segoe, sans-serif;">TAX INVOICE <br> فاتورة ضريبية</h3><b>
                </div>
                <div class="col-xs-6 " style="text-align: right;" >
                  <?php 
                   $inv_date = date("Y-m-d h:i:s", strtotime($inv[0]->date));
                   $vat=$inv[0]->new_vat - $inv[0]->vat_recovery;

                   $vat_total = $vat;//number_format((float)$vat, 2, '.', '');
                   $grand_total =number_format((float)$inv[0]->total_amount, 2, '.', '');
                   $seller = $biller->company;
                   $Vat_No = $biller->cf1;
                   $result = chr(1) . chr( strlen($seller) ) . $seller;
                   $result.= chr(2) . chr( strlen($Vat_No) ) . $Vat_No;
                   $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                   $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                   $result.= chr(5) . chr( strlen($vat_total) ) . $vat_total;
                   $newQr = base64_encode($result); 
                   ?>
                <?= $this->sim->qrcode('url', $newQr, 0); ?>
                </div>
                        
                 <div class="col-xs-12 padding010">
                 <!-- <div class="clearfix"></div> -->
                    <div class="table-responsive">
                        <table class="table  print-table order-table" style="font-family: Tahoma, Verdana, Segoe, sans-serif;">
                        <tr>
                             <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Invoice Date:</td>
                             <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=  date("d/m/Y", strtotime($inv[0]->date)); ?></td>
                             <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-right"><?=  date("d/m/Y", strtotime($inv[0]->date)); ?></td>
                             <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">تاريخ:</td>
                         </tr>
                        
                         <tr>
                             <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Project</td>
                             <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"> <?=$prooduct[0]->name?></td>
                             <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-right"> <?=$prooduct[0]->name_other?></td>
                             <td style="direction: rtl;ont-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">المشروع    :</td>
                         </tr>
                         <tr>
                                     <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Invoice Number:</td>
                                     <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"> <?= $inv[0]->invoice_id; ?> </td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"class="text-right"> <?= $inv[0]->invoice_id; ?> </td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">رقم الفاتورة:</td>
                         </tr>
                         <tr>
                             <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Work Order Reference No</td>
                             <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"> <?=$prooduct[0]->reference?></td>
                             <td class="text-right" style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"> <?=$prooduct[0]->reference?></td>
                             <td style="direction: rtl;" style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">رقم مرجع أمر العمل :</td>
                         </tr>
                       
                     </table>
                 </div>
                    
                         </div>
                       
                         <div class="col-xs-12 ">
                            <div class="table-responsive">
                                <table class="table  print-table order-table">
                                    <thead>
                                        <tr class="bg-dark">
                                            <th  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-left" colspan="2">Seller:</th>
                                            <th  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-right" colspan="2" style="direction: rtl;">تاجر:</th>
                                            <th  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-left" colspan="2">Buyer:</th>
                                            <th  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" class="text-right" colspan="2" style="direction: rtl;">مشتر:</th>
                                        </tr>
                                </thead>
                                <tbody>
                                 <tr >
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Name</td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><b><?= $biller->company && $biller->company != '-' ? $biller->company : $biller->name; ?></b></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><b><?= $biller->company_other && $biller->company_other != '-' ? $biller->company_other : $biller->name_other; ?></b></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">اسم:</td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Name</td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?= $customer->company && $customer->company != '-' ? $customer->company : $customer->name; ?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?= $customer->company_other && $customer->company_other != '-' ? $customer->company_other : $customer->name_other; ?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">اسم:</td>
                                 </tr>
                                
                                
                                
                                 
                                 <!-- <tr >
                                     <td style="font-size:10px">Phone </td>
                                     <td style="font-size:10px"><?=$biller->phone?></td>
                                     <td style="direction: rtl;font-size:10px"><?=$biller->phone?></td>
                                     <td style="direction: rtl;font-size:10px">هاتف:</td>
                                     <td style="font-size:10px">phone </td>
                                     <td  style="font-size:10px"><?=$customer->phone?></td>
                                     <td style="direction: rtl;font-size:10px"><?=$customer->phone?></td>
                                     <td style="direction: rtl;font-size:10px">هاتف:</td>
                                 </tr>
                                 <tr >
                                     <td style="font-size:10px">Email </td>
                                     <td style="font-size:10px"><?= $biller->email; ?></td>
                                     <td style="direction: rtl;font-size:10px"><?= $biller->email; ?></td>
                                     <td style="direction: rtl;font-size:10px">البريد الإلكتروني:</td>
                                     <td style="font-size:10px">Email </td>
                                     <td  style="font-size:10px"><?= $customer->email; ?></td>
                                     <td style="direction: rtl;font-size:10px"><?= $customer->email; ?></td>
                                     <td style="direction: rtl;font-size:10px">البريد الإلكتروني:</td>
                                 </tr> -->
                                 
                                
                                 <tr >
                                     <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">VAT No</td>
                                     <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" ><?= $biller->cf1?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?= $biller->cf1?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">ظريبه الشراء:</td>
                                     <td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">VAT No</td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=$customer->cf1?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=$customer->cf1?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">ظريبه الشراء:</td>
                                 </tr>
                                 <tr >
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Address </td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" ><?= $biller->address?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?= $biller->cf4?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">تبوك :</td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Address </td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" ><?=$customer->address?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=$customer->cf5?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">تبوك :</td>
                                 </tr>
                                 <tr >
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">Subject </td>
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="3" ><?= $inv[0]->subject ?></td>
                                   
                                     <td  style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="3" ><?= $inv[0]->subject_other ?></td>
                                     <td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">موضوع</td>
                                 </tr>
                                </tbody>
                             </table>
                           
                                <table class="table  print-table order-table">
                                   
                                <!-- <tr><td colspan="2">Gross Value</td><td colspan="2"><?=$inv[0]->gross_value?></td></tr> -->
                                 <!-- <tr><td colspan="2">Work Done Percentage</td><td colspan="2"><?=$inv[0]->work_done_percentage?></td><td style="direction: rtl;">نسبة العمل المنجز</td></tr> -->
                                 <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"colspan="2">Total Contractual Value</td><td colspan="2" style="direction: rtl"><?=number_format($inv[0]->gross_payment,2)?></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">إجمالي القيمة التعاقدية </td></tr>
                                 <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2"><?=$inv[0]->percentage_work_done?> % Work Done Amount</td><td colspan="2" style="direction: rtl"><?=number_format($inv[0]->work_done_amount,2)?></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"> <?=$inv[0]->percentage_work_done?>٪ كمية العمل المنجز    </td></tr>
                                
                                 <!-- <tr><td colspan="2"><?=$inv[0]->work_done_percentage?> % of Advance payment</td><td colspan="2"><?=($inv[0]->adavnce_payment_per_amount*$inv[0]->work_done_percentage)/100?></td><td style="direction: rtl;"> ٪ من الدفعة المقدمة</td></tr> -->
                                 <!-- <tr><td colspan="2"><?=$inv[0]->work_done_percentage?> % of Retention Amount</td><td colspan="2"><?=($inv[0]->retention_amt*$inv[0]->work_done_percentage)/100?></td><td style="direction: rtl;">10٪ من مبلغ الاحتفاظ</td></tr> -->
                                
                               
                                 <!-- <tr><td colspan="2">Vat of Net Payable</td><td colspan="2"><?=$inv[0]->vat_net_payable?></td></tr> -->
                                 
                                 <!-- <tr><td colspan="2">Total Amount To be Paid</td><td colspan="2"><?=$inv[0]->vat_net_payable + $inv[0]->net_payable?></td></tr> -->
                                 <!-- <tr><td colspan="2">Percentage of work done</td><td colspan="2"><?=$inv[0]->percentage_work_done?></td><td style="direction: rtl;">نسبة العمل المنجز</td></tr> -->
                                  
                                 <!-- <tr><td colspan="2">Work Done Amount</td><td colspan="2"><?=$inv[0]->gross_payment*$inv[0]->percentage_work_done?></td></tr> -->
                                 <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2"><b>Advance Deduction (<?=$inv[0]->adv_pay_per?> % of Advance Paid)</b></td><td colspan="2" style="direction: rtl"><b>(<?=number_format($inv[0]->advance_deduction,2)?>)</b></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">  الخصم المسبق( <?=$inv[0]->adv_pay_per?>٪ من الدفعة المقدمة)

 </td></tr>
                                 <!-- <tr><td colspan="2">Retention Percentage</td><td colspan="2"><?=$inv[0]->retantion_percentage?></td><td style="direction: rtl;">نسبة الاستبقاء</td></tr> -->
                                 <!-- <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2">Deduction for the progressive payments received</td><td colspan="2" style="direction: rtl">(<?=number_format($inv[0]->progressive_payment_amt_rec,2)?>)</td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">استقطاع المدفوعات التدريجية المستلمة</td></tr> -->
                                
                                 <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2"><b> <?=$inv[0]->retantion_percentage?> % Retention Amount</b></td><td colspan="2" style="direction: rtl"><b>(<?=number_format($inv[0]->retention_amount_calc,2)?>)</b></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"> <?=$inv[0]->retantion_percentage?>٪ مبلغ الاحتفاظ</td></tr>
                                 <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2">Net Payable after the deduction and retention</td><td colspan="2" style="direction: rtl"><?=number_format($inv[0]->net_payable,2)?></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">صافي المدفوعات بعد الخصم والاحتفاظ </td></tr>
                                 <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2"><?=$inv[0]->tax_rate?> % VAT for the Work Done Amount    </td><td colspan="2" style="direction: rtl"><?=number_format($inv[0]->new_vat,2)?></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><?=$inv[0]->tax_rate?> ٪ ضريبة القيمة المضافة على مبلغ العمل المنجز      
</td></tr>
                                 <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2"><b>VAT Recovery for the Advance Amount Paid</b></td><td colspan="2" style="direction: rtl"><b>(<?=number_format($inv[0]->vat_recovery,2)?>)</b></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">استرداد ضريبة القيمة المضافة للمبلغ المدفوع مقدمًا      
</td></tr>
									<!-- <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2">VAT Recovery for the Progressive Amount Paid</td><td colspan="2" style="direction: rtl">(<?=number_format($inv[0]->vat_recovery_prg,2)?>)</td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;">استرداد ضريبة القيمة المضافة للمبلغ التدريجي المدفوع</td></tr> -->
                                 <tr><td style="font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;" colspan="2"><b>Total Amount to be Paid including <?=$inv[0]->tax_rate?> % VAT</b></td><td colspan="2" style="direction: rtl"><b><?=number_format($inv[0]->total_amount,2)?></b></td><td style="direction: rtl;font-size:10px;font-family: Tahoma, Verdana, Segoe, sans-serif;"><b><?=$inv[0]->tax_rate?> %  المبلغ الإجمالي الذي  سيتم دفعه بما في ذلك

 </b></td></tr>
                                 <!-- <tr><td style="font-size:10px" colspan="2"><b>Total Amount In Words</b></td><td colspan="2"><b><?=$inv[0]->total_amount_in_words?></b></td><td style="direction: rtl;font-size:10px"><b> المبلغ الإجمالي بالكلمات</b></td></tr> -->
                               
                                </table>
                              <!--  <p>Best Regards</p> -->
                                <!-- <p><?=$prooduct[0]->regad_name?></p> -->
                                <!-- <p><?=$prooduct[0]->phone?></p> -->
                              <!--  <p><?=$prooduct[0]->designation?></p>-->
                             
                               <span >IBAN   
 : <?=$biller->cf3?>  رقم الحساب بصيغة</span>
								<br>
								 <span >CR NO   
 : <?=$biller->cf2?> رقم السجل التجاري
 </span>
    
        </div>
</div>
<script>
   $( document ).ready(function() {
   
    window.print();

});

    </script>

        