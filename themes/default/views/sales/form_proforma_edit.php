<?php
foreach ($tax_rates as $tax) {
    $tr[$tax->id] = $tax->name;
}
?>
<style type="text/css">
@media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px)  {
    /*#dyTable tbody td:nth-of-type(1):before { content: "<?= lang('no'); ?>"; }*/
    #dyTable tbody td:nth-of-type(2):before { content: "<?= lang('quantity'); ?>"; }
    #dyTable tbody td:nth-of-type(3):before { content: "<?= lang('product_code'); ?>"; }
    #dyTable tbody td:nth-of-type(4):before { content: "<?= lang('unit_price'); ?>"; }
    #dyTable tbody td:nth-of-type(5):before { content: "<?= lang('discount'); ?>"; }
    #dyTable tbody td:nth-of-type(6):before { content: "<?= lang('tax_rate'); ?>"; }
    #dyTable tbody td:nth-of-type(7):before { content: "<?= lang('subtotal'); ?>"; }
}
</style>

<div class="row">

    <div class="col-md-4">
        <div class="form-group">
            <label for="date"><?= lang("date"); ?></label>
            <?php $date = date('Y-m-d'); ?>
            <?= form_input('date',  date('d/m/Y', strtotime($inv[0]->date)) , 'class="form-control date" id="date"');?>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="billing_company"><?= lang("billing_company"); ?></label>
            <?php
            $bc[""] = lang("select")." ".lang("billing_company");
            foreach ($companies as $company) {
                $bu[$company->id] = $company->company;
            }
            echo form_dropdown('billing_company', $bu, (isset($_POST['billing_company']) ? $_POST['billing_company'] : ($inv ? $inv[0]->billing_company_id : '')), 'class="billing_company form-control" data-placeholder="'.lang("select")." ".lang("billing_company").'" id="billing_company"');
            ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="status"><?= lang("status"); ?></label>
            <?php
            if (!$q) {
                $st = array(
                    ''      => lang("select")." ".lang("status"),
                    'canceled' => lang('canceled'),
                    'overdue'   => lang('overdue'),
                    'paid'      => lang('paid'),
                    'pending'   => lang('pending')
                );
            } else {
                $st = array(
                    ''      => lang("select")." ".lang("status"),
                    'canceled' => lang('canceled'),
                    'ordered'   => lang('ordered'),
                    'pending'   => lang('pending'),
                    'sent'  => lang('sent')
                );
            }
            echo form_dropdown('status', $st, (isset($_POST['status']) ? $_POST['status'] : ($inv ? $inv[0]->status : '')), 'class="status form-control" data-placeholder="'.lang("select")." ".lang("status").'" id="status"');
            ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="customer"><?= lang("Project"); ?><span style="color:red"> *</span></label>
           
            <select class="form-control" name="project" id="project"  required>
                <option value="">Select</option>
                <?php if(!empty($project)){
            foreach ($project as $projects) { ?>
                <option <?php if($projects->id == $inv[0]->product_id){ echo "selected";} ?> value="<?= $projects->id?>" data-customer="<?=$projects->customer_id?>" data-retention="<?=$projects->retention_amount ?>" data-percentage="<?=$projects->adv_payment_percentage?>" data-apa="<?=$projects->adavnce_payment_per_amount?>" data-contractual="<?= $projects->price?>" ><?= $projects->name?></option>
                <?php } }?>
            </select>
                
            
           
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="customer"><?= lang("customer"); ?></label>
            <select class="form-control" name="customer" id="customer" >
                <option value="">Select</option>
                <?php if(!empty($customers)){
            foreach ($customers as $customerss) { ?>
                <option value="<?= $customerss->id?>" <?php if($customerss->id == $inv[0]->customer_id){ echo "selected";} ?> ><?= $customerss->company?></option>
                <?php } }?>
            </select>
        </div>
    </div>
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Subject"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text" value="<?=(isset($_POST['subject']) ? $_POST['subject'] : ($inv ? $inv[0]->subject : ''))?>"  class="form-control" name="subject" id="subject"> 
          
            <!-- <div class="clearfix"></div> -->
            <!-- </div> -->
        </div>
    </div>
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Subject In Other Language"); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text" value="<?=($inv ? $inv[0]->subject_other : '')?>" class="form-control" name="subject_other" id="subject_other"> 
          
            <!-- <div class="clearfix"></div> -->
            <!-- </div> -->
        </div>
    </div>
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Contractual Value"); ?></label>
         <input type="text" value="<?=$inv[0]->contractual_value ?>"  class="form-control" name="contractual_value" id="contractual_value">  
            
          
            </div>
        </div>
    
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Advance Payment Percentage "); ?></label>
            <!-- <div class="input-group"> -->
            <input type="text"  value="<?=$inv[0]->advance_payment_percentag ?>" class="form-control" name="adv_pay_per" id="adv_pay_per"> 
          
            <!-- <div class="clearfix"></div> -->
            <!-- </div> -->
        </div>
    </div>
    <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Advance Payment Percentage Amount"); ?></label>
            <!-- <div class="input-group"> -->
           
           
           
            <input type="text" value="<?=$inv[0]->adavnce_payment_per_amount?>" class="form-control" name="adv_pay_per_amount" id="adv_pay_per_amount">   
            <!-- <div class="clearfix"></div> -->
            <!-- </div> -->
        </div>
    </div>
     <div class="col-md-4 ">
        <div class="form-group">
            <label for="reference_no"><?= lang("Retention Amount"); ?></label> 
            <!-- <div class="input-group"> -->
            <input type="text" value="<?=(isset($_POST['retention_amt']) ? $_POST['retention_amt'] : ($inv ? $inv[0]->retention_amt : ''))?>" class="form-control" name="retention_amt" id="retention_amt">  
            <!-- <div class="clearfix"></div> -->
            <!-- </div> -->
        </div>
    </div>
    
    
    
   
    
</div>

