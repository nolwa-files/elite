<script>
	$(document).ready(function() {
		 $('#fileData').dataTable( {
		// 	"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		// 	"aaSorting": [[ 1, "desc" ]],
		// 	"iDisplayLength": 10,
		// 	"oTableTools": {
		// 		"sSwfPath": "<?= $assets; ?>media/swf/copy_csv_xls_pdf.swf",
		// 		"aButtons": [ "csv", "xls", { "sExtends": "pdf", "sPdfOrientation": "landscape", "sPdfMessage": "" }, "print" ]
		// 	},
		// 	"aoColumns": [ { "bSortable": false }, null, null, null, null, null, null, null, { "bSortable": false } ]
		 });
	});
</script>

<div class="page-head">
    <h2 class="pull-left"><?= $page_title; ?> <span class="page-meta"><?= lang("list_results_x"); ?></span> </h2>
</div>
<div class="clearfix"></div>
<div class="matter">
    <div class="container">
        <table id="fileData" cellpadding=0 cellspacing=10 class="table table-bordered table-condensed table-hover table-striped" style="margin-bottom: 5px;">
                     <thead>
                <tr class="active">
                  <th ><?=lang("Invoice No");?></th>
                    <th ><?=lang("date");?></th>
                    <th><?=lang("Type");?></th>
                    <th ><?=lang("Project");?></th>
                    
                   
                     <th ><?=lang("customer");?></th>
                     <th ><?=lang("Total Amount");?></th>
                      <th ><?=lang("Status");?></th> 
                   <th style="width:100px;"><?=lang("actions");?></th>
                </tr>
                </thead>
                <tbody>
             
              
                <?php 
                 
                
                $i=1;foreach($sales as $sales){ 
            
                    
                    
                    ?>
                    <tr>
                    <td><?=$sales->invoice_id?></td>
                         <td><?=$sales->date?></td>
                         <td><?=$sales->type?></td>
                        <td><?=$sales->code?> - <?=$sales->project_name?> </td>
                       
                        <td><?=$sales->company?></td>
                        <td><?=$sales->total_amount?></td>
                       <td><?=$sales->status?></td> 
                        <td>
                            <?php if($sales->type == "simple"){?>
                                
                                 <a  class="tip btn btn-info btn-xs" href="<?=base_url()?>sales/view_invoice_simple/<?=$sales->id?>"><i class="fa fa-eye"></i> </a>
                                 <a class="tip btn btn-primary btn-xs"  href="<?=base_url()?>sales/edit_simple?id=<?=$sales->id?>"> <i class="fa fa-edit"></i> </a> 
                            <?php }else if($sales->type == "progressive"){?>
                                <a  class="tip btn btn-info btn-xs" href="<?=base_url()?>sales/view_invoice_new/<?=$sales->id?>"><i class="fa fa-eye"></i> </a>
                                <a class="tip btn btn-primary btn-xs"  href="<?=base_url()?>sales/edit_sale?id=<?=$sales->id?>"> <i class="fa fa-edit"></i> </a> 
                        <?php }else if($sales->type == "proforma"){ ?> 
                            <a  class="tip btn btn-info btn-xs" href="<?=base_url()?>sales/view_invoice_proforma/<?=$sales->id?>"><i class="fa fa-eye"></i> </a> 
                            <a class="tip btn btn-primary btn-xs"  href="<?=base_url()?>sales/edit_proforma?id=<?=$sales->id?>"> <i class="fa fa-edit"></i> </a> 
                        <?php }else if($sales->type == "simpleprogressive"){ ?> 
                            <a  class="tip btn btn-info btn-xs" href="<?=base_url()?>sales/view_simple_progressive/<?=$sales->id?>"><i class="fa fa-eye"></i> </a>
                            <a class="tip btn btn-primary btn-xs"  href="<?=base_url()?>sales/edit_simple_progressive?id=<?=$sales->id?>"> <i class="fa fa-edit"></i> </a> 
                      
                        <?php }else if($sales->type == "advanceprogressive"){ ?> 
                            <a  class="tip btn btn-info btn-xs" href="<?=base_url()?>sales/view_advance_progressive/<?=$sales->id?>"><i class="fa fa-eye"></i> </a>
                            <a class="tip btn btn-primary btn-xs"  href="<?=base_url()?>sales/edit_advance_progressive?id=<?=$sales->id?>"> <i class="fa fa-edit"></i> </a> 
      
                            <?php }?>
                        <a class="tip btn btn-danger btn-xs" title="delete_sale" href="<?=base_url()?>sales/delete_sale/<?=$sales->id?>" onClick="return confirm(\''. lang('alert_x_user') .'\');">
                                <i class="fa fa-trash-o"></i>
                            </a>
                    </td>
                       


                    </tr>

                    <?php $i++; } ?>
</tbody>
               
               
            </table>
            <div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</div>


      