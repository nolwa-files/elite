<div class="page-head">
    <h2 class="pull-left"><?= $page_title; ?> <span class="page-meta"><?= lang("enter_info"); ?></span> </h2>
</div>
<div class="clearfix"></div>
<div class="matter">
    <div class="container">

        <?php
            $attrib = array('class' => 'form-horizontal');
            echo form_open("products/add");
        ?>
        <div class="form-group">
            <label for="date"><?= lang("Start Date"); ?></label>
            <?php $date = date('d/m/Y'); ?>
            <?= form_input('date', $date, 'class="form-control date" id="date"');?>
        </div>
 
        <div class="form-group">
            <label for="code"><?= lang("Project Code"); ?></label>
            <div class="controls"> <?= form_input('code', set_value('code', ''), 'class="form-control" id="code" maxlength="80"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="name"><?= lang("Project Name"); ?></label>
            <div class="controls"> <?= form_input('name', set_value('name', ''), 'class="form-control" id="name" maxlength="80"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="name"><?= lang("Name Other"); ?></label>
            <div class="controls"> <?= form_input('name_other', set_value('name_other', ''), 'class="form-control" id="name" maxlength="80"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="details"><?= lang("details"); ?></label>
            <div class="controls"> <?= form_textarea('details', set_value('details', ''), 'class="form-control" id="details" maxlength="255" style="height:60px;"'); ?>
            </div>
        </div>
       
        <div class="form-group">
            <label for="reference"><?= lang("Work Order Reference"); ?></label>
            <div class="controls"> <?= form_input('reference', set_value('reference', ''), 'class="form-control" id="reference" maxlength="80"');?>
            </div>
        </div>
        <!-- <div class="form-group">
            <label for="price"><?= lang("Contractual Value"); ?></label>
            <div class="controls"> <?= form_input('price', set_value('price', ''), 'class="form-control" id="price"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="price"><?= lang("Advance Payment Percentage"); ?></label>
            <div class="controls"> <?= form_input('adv_payment_percentage', set_value('adv_payment_percentage', ''), 'class="form-control" id="adv_payment_percentage"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="price"><?= lang("Retention Amount"); ?></label>
            <div class="controls"> <?= form_input('retention_amount', set_value('retention_amount', ''), 'class="form-control" id="retention_amount"');?>
            </div>
        </div> -->
        <div class="form-group">
            <label for="customer"><?= lang("Customer"); ?></label>
            <div class="controls"> 
                <select class="form-control" name="customer_id" id="customer_id">
                    <option value="">Select</option>
                    <?php if(!empty($customers)){ foreach($customers as $row){?>
                        <option value="<?=$row->id?>"><?=$row->name?></option>
                        <?php } }?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="reference"><?= lang("Name"); ?></label>
            <div class="controls"> <?= form_input('regad_name', set_value('regad_name', ''), 'class="form-control" id="regad_name" maxlength="80"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="reference"><?= lang("Number"); ?></label>
            <div class="controls"> <?= form_input('phone', set_value('phone', ''), 'class="form-control" id="phone" maxlength="80"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="reference"><?= lang("Designation"); ?></label>
            <div class="controls"> <?= form_input('designation', set_value('designation', ''), 'class="form-control" id="phone" maxlength="80"');?>
            </div>
        </div>
        <!-- <?php if ($Settings->default_tax_rate) { ?>
        <div class="form-group">
            <label for="tax_rate"><?= lang("tax_rate"); ?></label>
            <div class="controls">
            <?php
            foreach ($tax_rates as $rate) {
                $tr[$rate->id] = $rate->name;
            } ?>
            <?php echo form_dropdown('tax_rate', $tr, $Settings->default_tax_rate, 'class="form-control" id"tax_rate"'); ?>
            </div>
        </div>

        <div class="form-group">
            <?= lang('tax_method', 'tax_method'); ?>
            <?php $opts = array('exclusive' => lang('exclusive'), 'inclusive' => lang('inclusive')) ?>
            <?= form_dropdown('tax_method', $opts, set_value('tax_method'), 'class="form-control" id="tax_method"'); ?>
        </div>
        <?php } ?> -->
        

        <div class="form-group">
            <div class="controls"> <?= form_submit('submit', lang("add_product"), 'class="btn btn-primary"');?> </div>
        </div>
        <?= form_close();?>

        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
