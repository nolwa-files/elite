-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2022 at 11:39 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elite_style`
--

-- --------------------------------------------------------

--
-- Table structure for table `sim_calendar`
--

CREATE TABLE `sim_calendar` (
  `start` datetime NOT NULL,
  `title` varchar(55) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `end` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `color` varchar(7) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sim_company`
--

CREATE TABLE `sim_company` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `company` varchar(255) NOT NULL,
  `company_other` varchar(50) NOT NULL,
  `cf1` varchar(255) NOT NULL,
  `cf2` varchar(255) NOT NULL,
  `cf3` varchar(255) NOT NULL,
  `cf4` varchar(255) NOT NULL,
  `cf5` varchar(255) NOT NULL,
  `cf6` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(55) NOT NULL,
  `state` varchar(55) NOT NULL,
  `postal_code` varchar(8) NOT NULL,
  `country` varchar(55) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `logo` varchar(50) DEFAULT NULL,
  `ss_image` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_company`
--

INSERT INTO `sim_company` (`id`, `name`, `company`, `company_other`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `address`, `city`, `state`, `postal_code`, `country`, `phone`, `email`, `logo`, `ss_image`, `status`) VALUES
(1, 'Turki Hejab Al Harbi', 'Elite Style Contracting Establishment', 'مؤسسة ايليت ستايل للمقاولات', '300901196100003', '1010326479', 'SA8305000068201367324000, Alinma Bank', '', '', '', 'P O Box 12532, Riyadh, Saudi Arabia', 'Riyadh', 'Aleman  Alshafee', '12532', 'Saudi Arabia', '+966554292997', 'turki@estyleksa.org', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sim_customers`
--

CREATE TABLE `sim_customers` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `name_other` varchar(45) NOT NULL,
  `company` varchar(255) DEFAULT '-',
  `cf1` varchar(255) DEFAULT '-',
  `cf2` varchar(255) DEFAULT '-',
  `cf3` varchar(255) DEFAULT '-',
  `cf4` varchar(255) DEFAULT '-',
  `cf5` varchar(255) DEFAULT '-',
  `cf6` varchar(255) DEFAULT '-',
  `address` varchar(255) DEFAULT '-',
  `city` varchar(55) DEFAULT '-',
  `state` varchar(55) DEFAULT '-',
  `postal_code` varchar(8) DEFAULT '-',
  `country` varchar(55) DEFAULT '-',
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_customers`
--

INSERT INTO `sim_customers` (`id`, `name`, `name_other`, `company`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `address`, `city`, `state`, `postal_code`, `country`, `phone`, `email`) VALUES
(5, '', '', 'Shapoorji Pallonji Mideast LLC', '300145863200003', '', '', '', '', '', 'P O Box 300587, Riyadh, Saudi Arabia ', '-', '-', '-', '-', '', ''),
(6, '', '', 'Burj Rafal Real Estate Development Company ', '300194374200003', '', '', '', '', '', 'Riyadh, Saudi Arabia', '-', '-', '-', '-', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sim_date_format`
--

CREATE TABLE `sim_date_format` (
  `id` int(11) NOT NULL,
  `js` varchar(20) NOT NULL,
  `php` varchar(20) NOT NULL,
  `sql` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_date_format`
--

INSERT INTO `sim_date_format` (`id`, `js`, `php`, `sql`) VALUES
(1, 'mm-dd-yyyy', 'm-d-Y', '%m-%d-%Y'),
(2, 'mm/dd/yyyy', 'm/d/Y', '%m/%d/%Y'),
(3, 'mm.dd.yyyy', 'm.d.Y', '%m.%d.%Y'),
(4, 'dd-mm-yyyy', 'd-m-Y', '%d-%m-%Y'),
(5, 'dd/mm/yyyy', 'd/m/Y', '%d/%m/%Y'),
(6, 'dd.mm.yyyy', 'd.m.Y', '%d.%m.%Y');

-- --------------------------------------------------------

--
-- Table structure for table `sim_groups`
--

CREATE TABLE `sim_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_groups`
--

INSERT INTO `sim_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'sales', 'Sales Staff'),
(3, 'viewer', 'View Only User'),
(4, 'customer', 'Customers Group');

-- --------------------------------------------------------

--
-- Table structure for table `sim_login_attempts`
--

CREATE TABLE `sim_login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sim_migrations`
--

CREATE TABLE `sim_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_migrations`
--

INSERT INTO `sim_migrations` (`version`) VALUES
(2);

-- --------------------------------------------------------

--
-- Table structure for table `sim_notes`
--

CREATE TABLE `sim_notes` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `default_sale` tinyint(1) DEFAULT NULL,
  `default_quote` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_notes`
--

INSERT INTO `sim_notes` (`id`, `description`, `default_sale`, `default_quote`) VALUES
(1, 'This is test note.\r\nPlease update in settings > notes.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sim_payment`
--

CREATE TABLE `sim_payment` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `amount` decimal(25,2) NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(55) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_payment`
--

INSERT INTO `sim_payment` (`id`, `invoice_id`, `customer_id`, `date`, `note`, `amount`, `user`, `transaction_id`) VALUES
(12, 12, 1, '2021-12-10', 'Paid upon invoice', '1000.00', '2', NULL),
(13, 13, 1, '2021-12-10', 'Paid upon invoice', '800.00', '2', NULL),
(14, 14, 1, '2021-12-11', 'Paid upon invoice', '2.00', '2', NULL),
(15, 17, 1, '2021-12-11', 'Paid upon invoice', '5.00', '2', NULL),
(16, 21, 2, '2021-12-28', 'Paid upon invoice', '4000.00', '2', NULL),
(17, 22, 2, '2021-12-28', 'Paid upon invoice', '5110.00', '2', NULL),
(18, 2, 3, '2021-12-29', 'Paid upon invoice', '14.00', '2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sim_paypal`
--

CREATE TABLE `sim_paypal` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL,
  `fixed_charges` decimal(25,2) NOT NULL DEFAULT 2.00,
  `extra_charges_my` decimal(25,2) NOT NULL DEFAULT 3.90,
  `extra_charges_other` decimal(25,2) NOT NULL DEFAULT 4.40
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_paypal`
--

INSERT INTO `sim_paypal` (`id`, `active`, `account_email`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'mypaypal@paypal.com', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `sim_products`
--

CREATE TABLE `sim_products` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_other` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `reference` varchar(45) NOT NULL,
  `price` decimal(25,2) NOT NULL,
  `adv_payment_percentage` float NOT NULL,
  `retention_amount` float NOT NULL,
  `tax_rate` int(11) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `tax_method` varchar(20) DEFAULT 'exclusive',
  `customer_id` int(11) NOT NULL,
  `adavnce_payment_per_amount` float NOT NULL,
  `regad_name` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `designation` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_products`
--

INSERT INTO `sim_products` (`id`, `date`, `name`, `name_other`, `code`, `reference`, `price`, `adv_payment_percentage`, `retention_amount`, `tax_rate`, `details`, `tax_method`, `customer_id`, `adavnce_payment_per_amount`, `regad_name`, `phone`, `designation`) VALUES
(23, '2022-01-07', 'Renovation of Apartment unit 56-04', '', 'BRAC/56-04', 'PO-000054-1,  Dated 28/July/2021', '0.00', 0, 0, NULL, '', 'exclusive', 0, 0, '', '', ''),
(24, '2022-01-07', 'KAFD- J 03, Parcel 1.14', '', 'KAFD- J 03', 'WO KAFD/J03/00446/2021, Dated 21 Aug 2021', '0.00', 0, 0, NULL, '', 'exclusive', 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sim_quotes`
--

CREATE TABLE `sim_quotes` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(55) NOT NULL,
  `date` datetime NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,2) NOT NULL,
  `total_tax` decimal(25,2) NOT NULL,
  `grand_total` decimal(25,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shipping` decimal(25,2) DEFAULT 0.00,
  `order_discount_id` varchar(15) DEFAULT NULL,
  `total_discount` decimal(25,2) DEFAULT 0.00,
  `company_id` int(11) DEFAULT 1,
  `expiry_date` date DEFAULT NULL,
  `status` varchar(50) DEFAULT 'pending',
  `product_discount` decimal(15,2) DEFAULT NULL,
  `order_discount` decimal(15,2) DEFAULT NULL,
  `product_tax` decimal(15,2) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(15,2) DEFAULT NULL,
  `company_name` varchar(55) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sim_quote_items`
--

CREATE TABLE `sim_quote_items` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `quantity` decimal(25,2) NOT NULL,
  `unit_price` decimal(25,2) NOT NULL,
  `subtotal` decimal(25,2) NOT NULL,
  `tax_amt` decimal(25,2) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `net_unit_price` decimal(15,2) NOT NULL,
  `real_unit_price` decimal(15,2) NOT NULL,
  `discount` varchar(15) DEFAULT NULL,
  `discount_amt` decimal(15,2) DEFAULT NULL,
  `tax_method` varchar(20) DEFAULT 'exclusive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sim_sales`
--

CREATE TABLE `sim_sales` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(55) NOT NULL,
  `date` datetime NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,2) NOT NULL,
  `total_tax` decimal(25,2) NOT NULL,
  `grand_total` decimal(25,2) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'paid',
  `user_id` int(11) NOT NULL,
  `shipping` decimal(25,2) DEFAULT 0.00,
  `order_discount_id` varchar(15) DEFAULT NULL,
  `total_discount` decimal(25,2) DEFAULT 0.00,
  `company_id` int(11) DEFAULT 1,
  `due_date` date DEFAULT NULL,
  `recurring` tinyint(4) DEFAULT 0,
  `recur_date` date DEFAULT NULL,
  `paid` decimal(25,2) DEFAULT 0.00,
  `product_discount` decimal(15,2) DEFAULT NULL,
  `order_discount` decimal(15,2) DEFAULT NULL,
  `product_tax` decimal(15,2) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(15,2) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `contractual_value` float NOT NULL,
  `advance_payment_percentag` float NOT NULL,
  `retention_amt` float NOT NULL,
  `adavnce_payment_per_amount` float NOT NULL,
  `gross_value` float NOT NULL,
  `work_done_percentage` float NOT NULL,
  `net_payable` float NOT NULL,
  `vat_net_payable` float NOT NULL,
  `tax_rate` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_sales`
--

INSERT INTO `sim_sales` (`id`, `reference_no`, `product_id`, `customer_id`, `customer_name`, `date`, `user`, `note`, `total`, `total_tax`, `grand_total`, `status`, `user_id`, `shipping`, `order_discount_id`, `total_discount`, `company_id`, `due_date`, `recurring`, `recur_date`, `paid`, `product_discount`, `order_discount`, `product_tax`, `order_tax_id`, `order_tax`, `company_name`, `contractual_value`, `advance_payment_percentag`, `retention_amt`, `adavnce_payment_per_amount`, `gross_value`, `work_done_percentage`, `net_payable`, `vat_net_payable`, `tax_rate`) VALUES
(1, '2370634928111463', 1, 2, 'Admac', '2021-12-28 17:05:00', '2', NULL, '3000.00', '450.00', '3450.00', 'pending', 2, '0.00', NULL, '0.00', 1, '2021-12-28', 0, NULL, '0.00', '0.00', '0.00', '0.00', 2, '450.00', 'Elite Style Contracting Establishment', 25000, 10, 25000, 0, 0, 0, 0, 0, 0),
(2, '3374805017112935', 1, 3, 'harsha', '2021-12-29 13:20:00', '2', NULL, '14.00', '2.10', '16.10', 'paid', 2, '0.00', NULL, '0.00', 1, '2021-12-29', 0, NULL, '16.10', '0.00', '0.00', '0.00', 2, '2.10', 'Elite Style Contracting Establishment', 25000, 10, 25000, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sim_sale_items`
--

CREATE TABLE `sim_sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `quantity` decimal(25,2) NOT NULL,
  `unit_price` decimal(25,2) NOT NULL,
  `subtotal` decimal(25,2) NOT NULL,
  `tax_amt` decimal(25,2) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `net_unit_price` decimal(15,2) NOT NULL,
  `real_unit_price` decimal(15,2) NOT NULL,
  `discount` varchar(15) DEFAULT NULL,
  `discount_amt` decimal(15,2) DEFAULT NULL,
  `tax_method` varchar(20) DEFAULT 'exclusive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_sale_items`
--

INSERT INTO `sim_sale_items` (`id`, `sale_id`, `product_name`, `tax_rate_id`, `tax`, `quantity`, `unit_price`, `subtotal`, `tax_amt`, `details`, `net_unit_price`, `real_unit_price`, `discount`, `discount_amt`, `tax_method`) VALUES
(1, 1, 'test1', NULL, '0', '1.00', '1000.00', '1000.00', '0.00', '', '1000.00', '1000.00', NULL, '0.00', 'exclusive'),
(2, 1, 'test2', NULL, '0', '1.00', '2000.00', '2000.00', '0.00', '', '2000.00', '2000.00', NULL, '0.00', 'exclusive'),
(3, 2, 'ghgfhg', NULL, '0', '1.00', '14.00', '14.00', '0.00', '', '14.00', '14.00', NULL, '0.00', 'exclusive');

-- --------------------------------------------------------

--
-- Table structure for table `sim_sale_new`
--

CREATE TABLE `sim_sale_new` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(45) NOT NULL,
  `date` date DEFAULT NULL,
  `subject` text NOT NULL,
  `subject_other` text CHARACTER SET utf8 NOT NULL,
  `progressive_payment_amt_rec` double NOT NULL,
  `progressive_payment_certified_amount` double DEFAULT NULL,
  `user` int(11) NOT NULL,
  `billing_company_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `reference_no` varchar(45) NOT NULL,
  `product_id` int(11) NOT NULL,
  `contractual_value` double NOT NULL,
  `adv_pay_per` double NOT NULL,
  `retention_amt` double NOT NULL,
  `advance_payment_percentag` double NOT NULL,
  `adavnce_payment_per_amount` double NOT NULL,
  `gross_value` double NOT NULL,
  `work_done_percentage` double NOT NULL,
  `net_payable` double NOT NULL,
  `vat_net_payable` double NOT NULL,
  `tax_rate` double NOT NULL,
  `percentage_work_done` double NOT NULL,
  `gross_payment` double NOT NULL,
  `work_done_amount` double NOT NULL,
  `advance_deduction` double NOT NULL,
  `retantion_percentage` double NOT NULL,
  `retention_amount_calc` double NOT NULL,
  `new_vat` double NOT NULL,
  `vat_recovery` double NOT NULL,
  `vat_recovery_prg` double NOT NULL,
  `total_amount` double NOT NULL,
  `total_amount_in_words` text DEFAULT NULL,
  `status` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sim_sale_new`
--

INSERT INTO `sim_sale_new` (`id`, `invoice_id`, `date`, `subject`, `subject_other`, `progressive_payment_amt_rec`, `progressive_payment_certified_amount`, `user`, `billing_company_id`, `customer_id`, `reference_no`, `product_id`, `contractual_value`, `adv_pay_per`, `retention_amt`, `advance_payment_percentag`, `adavnce_payment_per_amount`, `gross_value`, `work_done_percentage`, `net_payable`, `vat_net_payable`, `tax_rate`, `percentage_work_done`, `gross_payment`, `work_done_amount`, `advance_deduction`, `retantion_percentage`, `retention_amount_calc`, `new_vat`, `vat_recovery`, `vat_recovery_prg`, `total_amount`, `total_amount_in_words`, `status`, `type`) VALUES
(24, 'INV001', '2022-01-18', 'fgfg', 'fgfg', 0, NULL, 0, 1, 6, '', 23, 0, 30, 4612.5, 30, 55350, 0, 25, 27675, 0, 15, 25, 184500, 46125, 13837.5, 10, 4612.5, 6918.75, 2075.625, 0, 32518.125, 'Thirty-Two Thousand Five Hundred and Eightteen', 'pending', 'advanceprogressive'),
(25, 'INV003', '2022-01-18', '', '', 23000, NULL, 0, 1, 6, '', 24, 0, 0, 0, 0, 0, 0, 0, 17250, 0, 15, 0, 0, 57500, 17250, 0, 0, 2587.5, 0, 0, 19837.5, 'Nineteen Thousand Eight Hundred and Thirty-Seven', 'pending', 'simpleprogressive'),
(26, 'INV004', '2022-01-18', '', '', 0, NULL, 0, 1, 5, '', 23, 184500, 30, 1000, 30, 1000, 182500, 0, 0, 0, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1150, 'One Thousand One Hundred and Fifty', 'overdue', 'proforma');

-- --------------------------------------------------------

--
-- Table structure for table `sim_sessions`
--

CREATE TABLE `sim_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_sessions`
--

INSERT INTO `sim_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('11jvgu2gfle6hc0ia8j52q11qcnmfl7q', '::1', 1642497542, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323439373534323b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226f6c64223b7d),
('1vkt1a16eqjoav675h839voa8br9u416', '::1', 1642501974, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323530313937343b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b),
('2q742ug2pi1olu98oj20ave4rb5tnb6v', '::1', 1642498548, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323439383534383b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b),
('cj46r06mh7qpc23lnh4eb4va1tcuck9q', '::1', 1642500296, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323530303239363b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b),
('fg292ligupjhupbi9qgb710ldb3isujn', '::1', 1642498849, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323439383834393b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b),
('fqfcjiio8jc6auhip46t8t8jb98l35fh', '::1', 1642674773, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323637343735383b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432343936373337223b637573746f6d65725f69647c4e3b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226f6c64223b7d),
('jki2ioi6hi6i9qpu30qc8pck43cd3i6j', '::1', 1642499155, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323439393135353b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b),
('l6rnchsbhpt1jtbta6one26srm6ctpj6', '::1', 1642501226, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323530313232363b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b),
('s52q99hkj92p4dsikf34i4q2ck6mecfe', '::1', 1642502193, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323530313937343b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b),
('t2778b72ovs9tkf0imq9ka70vb32mpjo', '::1', 1642498215, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323439383231353b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b),
('t9n5qm3h1q1pjds65673kql7dh39g2bv', '::1', 1642497241, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323439373234313b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b),
('tcv9igd0gf1sl4q1iil4ls72lnv3o9jp', '::1', 1642499982, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323439393938323b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b),
('tr1s392qihrfie4mjqakj316mg25ck46', '::1', 1642497859, 0x5f5f63695f6c6173745f726567656e65726174657c693a313634323439373835393b6964656e746974797c733a31393a227475726b6940657374796c656b73612e6f7267223b757365726e616d657c733a353a22456c697465223b66697273745f6e616d657c733a32303a225475726b692048656a616220416c204861726269223b656d61696c7c733a31393a227475726b6940657374796c656b73612e6f7267223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363432333933323730223b637573746f6d65725f69647c4e3b);

-- --------------------------------------------------------

--
-- Table structure for table `sim_settings`
--

CREATE TABLE `sim_settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `invoice_logo` varchar(50) NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `language` varchar(20) NOT NULL,
  `currency_prefix` varchar(3) NOT NULL,
  `default_tax_rate` int(2) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `no_of_rows` int(2) NOT NULL,
  `total_rows` int(2) NOT NULL,
  `dateformat` tinyint(4) NOT NULL,
  `print_payment` tinyint(4) NOT NULL,
  `calendar` tinyint(4) NOT NULL,
  `restrict_sales` tinyint(4) NOT NULL,
  `major` varchar(25) DEFAULT 'Dollars',
  `minor` varchar(25) DEFAULT 'Cents',
  `display_words` tinyint(4) DEFAULT 0,
  `customer_user` tinyint(1) DEFAULT 0,
  `version` varchar(10) DEFAULT '2.2',
  `email_html` tinyint(1) DEFAULT 1,
  `protocol` varchar(20) DEFAULT 'mail',
  `mailpath` varchar(55) DEFAULT '',
  `smtp_host` varchar(55) DEFAULT NULL,
  `smtp_user` varchar(55) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(10) DEFAULT NULL,
  `smtp_crypto` varchar(10) DEFAULT NULL,
  `thousands_sep` varchar(10) DEFAULT ',',
  `decimals_sep` varchar(10) DEFAULT '.',
  `decimals` tinyint(4) DEFAULT 2,
  `default_email` varchar(55) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT NULL,
  `envato_username` varchar(55) DEFAULT NULL,
  `theme` varchar(20) NOT NULL DEFAULT 'default',
  `product_discount` tinyint(1) DEFAULT NULL,
  `barcode_img` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_settings`
--

INSERT INTO `sim_settings` (`setting_id`, `logo`, `invoice_logo`, `site_name`, `language`, `currency_prefix`, `default_tax_rate`, `rows_per_page`, `no_of_rows`, `total_rows`, `dateformat`, `print_payment`, `calendar`, `restrict_sales`, `major`, `minor`, `display_words`, `customer_user`, `version`, `email_html`, `protocol`, `mailpath`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `smtp_crypto`, `thousands_sep`, `decimals_sep`, `decimals`, `default_email`, `purchase_code`, `envato_username`, `theme`, `product_discount`, `barcode_img`) VALUES
(1, 'logo.png', 'logo1.png', 'Elite Style', 'english', 'SR', 0, 10, 2, 3, 5, 0, 0, 0, 'Dollars', 'Cents', 1, 1, '3.7.2', 0, 'mail', '', '', '', '', '', NULL, ',', '.', 2, 'info@nolwa.com', NULL, NULL, 'default', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sim_skrill`
--

CREATE TABLE `sim_skrill` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL DEFAULT 'testaccount2@moneybookers.com',
  `secret_word` varchar(20) NOT NULL DEFAULT 'mbtest',
  `fixed_charges` decimal(25,2) NOT NULL DEFAULT 0.00,
  `extra_charges_my` decimal(25,2) NOT NULL DEFAULT 0.00,
  `extra_charges_other` decimal(25,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_skrill`
--

INSERT INTO `sim_skrill` (`id`, `active`, `account_email`, `secret_word`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'testaccount2@moneybookers.com', 'mbtest', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `sim_stripe`
--

CREATE TABLE `sim_stripe` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `secret_key` varchar(191) NOT NULL,
  `publishable_key` varchar(191) NOT NULL,
  `fixed_charges` decimal(25,2) NOT NULL DEFAULT 2.00,
  `extra_charges_my` decimal(25,2) NOT NULL DEFAULT 3.90,
  `extra_charges_other` decimal(25,2) NOT NULL DEFAULT 4.40
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_stripe`
--

INSERT INTO `sim_stripe` (`id`, `active`, `secret_key`, `publishable_key`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'sk_test_jHf4cEzAYtgcXvgWPCsQAn50', 'pk_test_beat8SWPORb0OVdF2H77A7uG', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `sim_tax_rates`
--

CREATE TABLE `sim_tax_rates` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `rate` decimal(4,2) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_tax_rates`
--

INSERT INTO `sim_tax_rates` (`id`, `name`, `rate`, `type`) VALUES
(2, 'VAT @15%', '15.00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sim_users`
--

CREATE TABLE `sim_users` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `vat_no` varchar(30) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_users`
--

INSERT INTO `sim_users` (`id`, `ip_address`, `username`, `password`, `vat_no`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `customer_id`) VALUES
(2, 0xcaa489eb, 'Elite', '114d10c55dbc73261075b660c70298dbe3fb8b94', '', NULL, 'turki@estyleksa.org', NULL, NULL, NULL, NULL, 1639136426, 1642674773, 1, 'Turki Hejab Al Harbi', 'Rahees', 'Elite Style Contracting Establishment', '+966 50 422 6829', NULL),
(6, 0x5da93fb0, '', '1a8c3c9fb36d25529fa099fa281fd339edcbe4d2', '', NULL, '', NULL, NULL, NULL, NULL, 1641538775, 1641538775, 1, '', NULL, 'Burj Rafal Real Estate Development Company ', '', 6);

-- --------------------------------------------------------

--
-- Table structure for table `sim_users_groups`
--

CREATE TABLE `sim_users_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_users_groups`
--

INSERT INTO `sim_users_groups` (`id`, `user_id`, `group_id`) VALUES
(2, 2, 1),
(3, 3, 4),
(4, 4, 4),
(5, 5, 4),
(6, 6, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sim_calendar`
--
ALTER TABLE `sim_calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_company`
--
ALTER TABLE `sim_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_customers`
--
ALTER TABLE `sim_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_date_format`
--
ALTER TABLE `sim_date_format`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_groups`
--
ALTER TABLE `sim_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_login_attempts`
--
ALTER TABLE `sim_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_notes`
--
ALTER TABLE `sim_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_payment`
--
ALTER TABLE `sim_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_paypal`
--
ALTER TABLE `sim_paypal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_products`
--
ALTER TABLE `sim_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_quotes`
--
ALTER TABLE `sim_quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_quote_items`
--
ALTER TABLE `sim_quote_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_sales`
--
ALTER TABLE `sim_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_sale_items`
--
ALTER TABLE `sim_sale_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_sale_new`
--
ALTER TABLE `sim_sale_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_sessions`
--
ALTER TABLE `sim_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `sim_settings`
--
ALTER TABLE `sim_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `sim_skrill`
--
ALTER TABLE `sim_skrill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_stripe`
--
ALTER TABLE `sim_stripe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_tax_rates`
--
ALTER TABLE `sim_tax_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_users`
--
ALTER TABLE `sim_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_users_groups`
--
ALTER TABLE `sim_users_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sim_calendar`
--
ALTER TABLE `sim_calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `sim_company`
--
ALTER TABLE `sim_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sim_customers`
--
ALTER TABLE `sim_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sim_date_format`
--
ALTER TABLE `sim_date_format`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sim_groups`
--
ALTER TABLE `sim_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sim_login_attempts`
--
ALTER TABLE `sim_login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `sim_notes`
--
ALTER TABLE `sim_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sim_payment`
--
ALTER TABLE `sim_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sim_products`
--
ALTER TABLE `sim_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `sim_quotes`
--
ALTER TABLE `sim_quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sim_quote_items`
--
ALTER TABLE `sim_quote_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sim_sales`
--
ALTER TABLE `sim_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sim_sale_items`
--
ALTER TABLE `sim_sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sim_sale_new`
--
ALTER TABLE `sim_sale_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `sim_tax_rates`
--
ALTER TABLE `sim_tax_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sim_users`
--
ALTER TABLE `sim_users`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sim_users_groups`
--
ALTER TABLE `sim_users_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
