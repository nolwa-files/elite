-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 11, 2021 at 04:16 PM
-- Server version: 5.7.36-log
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `orbitbea_manah_uniformdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `sim_calendar`
--

CREATE TABLE `sim_calendar` (
  `start` datetime NOT NULL,
  `title` varchar(55) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `end` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `color` varchar(7) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sim_company`
--

CREATE TABLE `sim_company` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `company` varchar(255) NOT NULL,
  `cf1` varchar(255) NOT NULL,
  `cf2` varchar(255) NOT NULL,
  `cf3` varchar(255) NOT NULL,
  `cf4` varchar(255) NOT NULL,
  `cf5` varchar(255) NOT NULL,
  `cf6` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(55) NOT NULL,
  `state` varchar(55) NOT NULL,
  `postal_code` varchar(8) NOT NULL,
  `country` varchar(55) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `logo` varchar(50) DEFAULT NULL,
  `ss_image` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_company`
--

INSERT INTO `sim_company` (`id`, `name`, `company`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `address`, `city`, `state`, `postal_code`, `country`, `phone`, `email`, `logo`, `ss_image`, `status`) VALUES
(1, 'Maana Uniforms', 'Mana Trading EST', '300160368100003', '', '', '', '', '', 'Door No: 5/2396, Ground Floor, Al Harsh Planet View. KuniyilKavu Road- Calicut.', 'Calicut', 'Kerala', '673001', 'India', '09526343232', 'manahtrdg@gmail.com', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sim_customers`
--

CREATE TABLE `sim_customers` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `company` varchar(255) DEFAULT '-',
  `cf1` varchar(255) DEFAULT '-',
  `cf2` varchar(255) DEFAULT '-',
  `cf3` varchar(255) DEFAULT '-',
  `cf4` varchar(255) DEFAULT '-',
  `cf5` varchar(255) DEFAULT '-',
  `cf6` varchar(255) DEFAULT '-',
  `address` varchar(255) DEFAULT '-',
  `city` varchar(55) DEFAULT '-',
  `state` varchar(55) DEFAULT '-',
  `postal_code` varchar(8) DEFAULT '-',
  `country` varchar(55) DEFAULT '-',
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_customers`
--

INSERT INTO `sim_customers` (`id`, `name`, `company`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `address`, `city`, `state`, `postal_code`, `country`, `phone`, `email`) VALUES
(1, 'Cash', 'Cash', '', '', '', '', '', '', 'Customer Address', 'City', 'State', '46050', 'Country', '0123456789', 'customer@nolwa.com');

-- --------------------------------------------------------

--
-- Table structure for table `sim_date_format`
--

CREATE TABLE `sim_date_format` (
  `id` int(11) NOT NULL,
  `js` varchar(20) NOT NULL,
  `php` varchar(20) NOT NULL,
  `sql` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_date_format`
--

INSERT INTO `sim_date_format` (`id`, `js`, `php`, `sql`) VALUES
(1, 'mm-dd-yyyy', 'm-d-Y', '%m-%d-%Y'),
(2, 'mm/dd/yyyy', 'm/d/Y', '%m/%d/%Y'),
(3, 'mm.dd.yyyy', 'm.d.Y', '%m.%d.%Y'),
(4, 'dd-mm-yyyy', 'd-m-Y', '%d-%m-%Y'),
(5, 'dd/mm/yyyy', 'd/m/Y', '%d/%m/%Y'),
(6, 'dd.mm.yyyy', 'd.m.Y', '%d.%m.%Y');

-- --------------------------------------------------------

--
-- Table structure for table `sim_groups`
--

CREATE TABLE `sim_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_groups`
--

INSERT INTO `sim_groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'sales', 'Sales Staff'),
(3, 'viewer', 'View Only User'),
(4, 'customer', 'Customers Group');

-- --------------------------------------------------------

--
-- Table structure for table `sim_login_attempts`
--

CREATE TABLE `sim_login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_login_attempts`
--

INSERT INTO `sim_login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, 0x00000000000000000000000000000001, 'contact@nolwa.com', 1639109009),
(2, 0x00000000000000000000000000000001, 'info@nolwa.com', 1639109021),
(3, 0x00000000000000000000000000000001, 'info@nolwa.com', 1639109031),
(4, 0x00000000000000000000000000000001, 'info@orbitbeam.net', 1639109044),
(5, 0x00000000000000000000000000000001, 'contact@nolwa.com', 1639126850),
(6, 0x00000000000000000000000000000001, 'contact@nolwa.com', 1639126850),
(7, 0x2a0209b00012560f00010000f3f75d90, 'rahees@orbitbeam.nt', 1639141516);

-- --------------------------------------------------------

--
-- Table structure for table `sim_migrations`
--

CREATE TABLE `sim_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_migrations`
--

INSERT INTO `sim_migrations` (`version`) VALUES
(2);

-- --------------------------------------------------------

--
-- Table structure for table `sim_notes`
--

CREATE TABLE `sim_notes` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `default_sale` tinyint(1) DEFAULT NULL,
  `default_quote` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_notes`
--

INSERT INTO `sim_notes` (`id`, `description`, `default_sale`, `default_quote`) VALUES
(1, 'This is test note.\r\nPlease update in settings > notes.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sim_payment`
--

CREATE TABLE `sim_payment` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `amount` decimal(25,2) NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(55) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_payment`
--

INSERT INTO `sim_payment` (`id`, `invoice_id`, `customer_id`, `date`, `note`, `amount`, `user`, `transaction_id`) VALUES
(12, 12, 1, '2021-12-10', 'Paid upon invoice', 1000.00, '2', NULL),
(13, 13, 1, '2021-12-10', 'Paid upon invoice', 800.00, '2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sim_paypal`
--

CREATE TABLE `sim_paypal` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL,
  `fixed_charges` decimal(25,2) NOT NULL DEFAULT '2.00',
  `extra_charges_my` decimal(25,2) NOT NULL DEFAULT '3.90',
  `extra_charges_other` decimal(25,2) NOT NULL DEFAULT '4.40'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_paypal`
--

INSERT INTO `sim_paypal` (`id`, `active`, `account_email`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'mypaypal@paypal.com', 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `sim_products`
--

CREATE TABLE `sim_products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` decimal(25,2) NOT NULL,
  `tax_rate` int(11) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `tax_method` varchar(20) DEFAULT 'exclusive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sim_quotes`
--

CREATE TABLE `sim_quotes` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(55) NOT NULL,
  `date` datetime NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,2) NOT NULL,
  `total_tax` decimal(25,2) NOT NULL,
  `grand_total` decimal(25,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shipping` decimal(25,2) DEFAULT '0.00',
  `order_discount_id` varchar(15) DEFAULT NULL,
  `total_discount` decimal(25,2) DEFAULT '0.00',
  `company_id` int(11) DEFAULT '1',
  `expiry_date` date DEFAULT NULL,
  `status` varchar(50) DEFAULT 'pending',
  `product_discount` decimal(15,2) DEFAULT NULL,
  `order_discount` decimal(15,2) DEFAULT NULL,
  `product_tax` decimal(15,2) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(15,2) DEFAULT NULL,
  `company_name` varchar(55) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sim_quote_items`
--

CREATE TABLE `sim_quote_items` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `quantity` decimal(25,2) NOT NULL,
  `unit_price` decimal(25,2) NOT NULL,
  `subtotal` decimal(25,2) NOT NULL,
  `tax_amt` decimal(25,2) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `net_unit_price` decimal(15,2) NOT NULL,
  `real_unit_price` decimal(15,2) NOT NULL,
  `discount` varchar(15) DEFAULT NULL,
  `discount_amt` decimal(15,2) DEFAULT NULL,
  `tax_method` varchar(20) DEFAULT 'exclusive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sim_sales`
--

CREATE TABLE `sim_sales` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(55) NOT NULL,
  `date` datetime NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,2) NOT NULL,
  `total_tax` decimal(25,2) NOT NULL,
  `grand_total` decimal(25,2) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'paid',
  `user_id` int(11) NOT NULL,
  `shipping` decimal(25,2) DEFAULT '0.00',
  `order_discount_id` varchar(15) DEFAULT NULL,
  `total_discount` decimal(25,2) DEFAULT '0.00',
  `company_id` int(11) DEFAULT '1',
  `due_date` date DEFAULT NULL,
  `recurring` tinyint(4) DEFAULT '0',
  `recur_date` date DEFAULT NULL,
  `paid` decimal(25,2) DEFAULT '0.00',
  `product_discount` decimal(15,2) DEFAULT NULL,
  `order_discount` decimal(15,2) DEFAULT NULL,
  `product_tax` decimal(15,2) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(15,2) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_sales`
--

INSERT INTO `sim_sales` (`id`, `reference_no`, `customer_id`, `customer_name`, `date`, `user`, `note`, `total`, `total_tax`, `grand_total`, `status`, `user_id`, `shipping`, `order_discount_id`, `total_discount`, `company_id`, `due_date`, `recurring`, `recur_date`, `paid`, `product_discount`, `order_discount`, `product_tax`, `order_tax_id`, `order_tax`, `company_name`) VALUES
(12, '', 1, 'Cash', '2021-12-10 18:07:00', '2', '', 1000.00, 150.00, 1150.00, 'paid', 2, 0.00, NULL, 0.00, 1, '2021-12-10', 0, NULL, 1150.00, 0.00, 0.00, 0.00, 2, 150.00, 'Mana Trading EST'),
(13, '', 1, 'Cash', '2021-12-10 18:10:00', '2', '', 800.00, 120.00, 920.00, 'paid', 2, 0.00, NULL, 0.00, 1, '2021-12-10', 0, NULL, 920.00, 0.00, 0.00, 0.00, 2, 120.00, 'Mana Trading EST');

-- --------------------------------------------------------

--
-- Table structure for table `sim_sale_items`
--

CREATE TABLE `sim_sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `quantity` decimal(25,2) NOT NULL,
  `unit_price` decimal(25,2) NOT NULL,
  `subtotal` decimal(25,2) NOT NULL,
  `tax_amt` decimal(25,2) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `net_unit_price` decimal(15,2) NOT NULL,
  `real_unit_price` decimal(15,2) NOT NULL,
  `discount` varchar(15) DEFAULT NULL,
  `discount_amt` decimal(15,2) DEFAULT NULL,
  `tax_method` varchar(20) DEFAULT 'exclusive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_sale_items`
--

INSERT INTO `sim_sale_items` (`id`, `sale_id`, `product_name`, `tax_rate_id`, `tax`, `quantity`, `unit_price`, `subtotal`, `tax_amt`, `details`, `net_unit_price`, `real_unit_price`, `discount`, `discount_amt`, `tax_method`) VALUES
(18, 12, 'Pant', NULL, '0', 2.00, 500.00, 1000.00, 0.00, '', 500.00, 500.00, NULL, 0.00, 'exclusive'),
(19, 13, 'Pant', NULL, '0', 2.00, 250.00, 500.00, 0.00, '', 250.00, 250.00, NULL, 0.00, 'exclusive'),
(20, 13, 'Shirt', NULL, '0', 2.00, 150.00, 300.00, 0.00, '', 150.00, 150.00, NULL, 0.00, 'exclusive');

-- --------------------------------------------------------

--
-- Table structure for table `sim_sessions`
--

CREATE TABLE `sim_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_sessions`
--

INSERT INTO `sim_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('353c521dc488e3b21f96e5588d8a4144c54f6a0b', '202.164.137.235', 1639141240, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393134313234303b),
('46bd42861a6bcb578b3c51880abe3ad609d8927b', '2a02:9b0:12:560f:1:0:f3f7:5d90', 1639142052, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393134323035323b6964656e746974797c733a32303a22726168656573406f726269746265616d2e6e6574223b757365726e616d657c733a31353a226d7568616d6d616420726168656573223b66697273745f6e616d657c733a383a224d7568616d6d6164223b656d61696c7c733a32303a22726168656573406f726269746265616d2e6e6574223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313430393438223b637573746f6d65725f69647c4e3b),
('4c692c1c7a3ad03d058b507fe455513dc230eb6d', '2a02:9b0:12:560f:1:0:f3f7:5d90', 1639141748, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393134313734383b6964656e746974797c733a32303a22726168656573406f726269746265616d2e6e6574223b757365726e616d657c733a31353a226d7568616d6d616420726168656573223b66697273745f6e616d657c733a383a224d7568616d6d6164223b656d61696c7c733a32303a22726168656573406f726269746265616d2e6e6574223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313430393438223b637573746f6d65725f69647c4e3b),
('4ed2a742670ea809c71f95e4faabc6bc3724a0e6', '202.164.137.235', 1639136742, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133363734323b6964656e746974797c733a32303a22726168656573406f726269746265616d2e6e6574223b757365726e616d657c733a31353a226d7568616d6d616420726168656573223b66697273745f6e616d657c733a383a224d7568616d6d6164223b656d61696c7c733a32303a22726168656573406f726269746265616d2e6e6574223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313336343236223b637573746f6d65725f69647c4e3b),
('4fa27f2c74411e939c825fa1b875be9ef8961f06', '202.164.137.235', 1639140825, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393134303832353b6964656e746974797c733a32303a22726168656573406f726269746265616d2e6e6574223b757365726e616d657c733a31353a226d7568616d6d616420726168656573223b66697273745f6e616d657c733a383a224d7568616d6d6164223b656d61696c7c733a32303a22726168656573406f726269746265616d2e6e6574223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313339343434223b637573746f6d65725f69647c4e3b),
('5k4dgudisv0ei3f4af1c3dsrls8d9t1g', '::1', 1639133976, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133333936363b6964656e746974797c733a31383a2261646d696e4074656364696172792e636f6d223b757365726e616d657c733a31333a2241646d696e6973747261746f72223b66697273745f6e616d657c733a353a2241646d696e223b656d61696c7c733a31383a2261646d696e4074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313039303539223b637573746f6d65725f69647c4e3b),
('763ihqd7bmcq2r8ojqmsgg7dv3kasmfe', '::1', 1639132535, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133323533353b6964656e746974797c733a31383a2261646d696e4074656364696172792e636f6d223b757365726e616d657c733a31333a2241646d696e6973747261746f72223b66697273745f6e616d657c733a353a2241646d696e223b656d61696c7c733a31383a2261646d696e4074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313039303539223b637573746f6d65725f69647c4e3b),
('8fef1e8a8c33ea0cd6e4aa9a577731ea99297438', '202.164.137.235', 1639141168, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393134313136313b),
('8u3re5apo7vagmq4co4rtldrcq0hmlv6', '::1', 1639133545, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133333534353b6964656e746974797c733a31383a2261646d696e4074656364696172792e636f6d223b757365726e616d657c733a31333a2241646d696e6973747261746f72223b66697273745f6e616d657c733a353a2241646d696e223b656d61696c7c733a31383a2261646d696e4074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313039303539223b637573746f6d65725f69647c4e3b),
('91cb520a06748c91319d7b16974614b33c103d85', '202.164.137.235', 1639139674, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133393637343b6964656e746974797c733a32303a22726168656573406f726269746265616d2e6e6574223b757365726e616d657c733a31353a226d7568616d6d616420726168656573223b66697273745f6e616d657c733a383a224d7568616d6d6164223b656d61696c7c733a32303a22726168656573406f726269746265616d2e6e6574223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313338363532223b637573746f6d65725f69647c4e3b),
('a22bf9c444e62594a51f5ed96129f024acd30fc9', '202.164.137.235', 1639139251, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133393235313b6964656e746974797c733a32303a22726168656573406f726269746265616d2e6e6574223b757365726e616d657c733a31353a226d7568616d6d616420726168656573223b66697273745f6e616d657c733a383a224d7568616d6d6164223b656d61696c7c733a32303a22726168656573406f726269746265616d2e6e6574223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313338363134223b637573746f6d65725f69647c4e3b),
('b24ff2a43c6786c34917a434bf879ac7e16b31ce', '2a02:9b0:12:560f:1:0:f3f7:5d90', 1639142171, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393134323035323b6964656e746974797c733a32303a22726168656573406f726269746265616d2e6e6574223b757365726e616d657c733a31353a226d7568616d6d616420726168656573223b66697273745f6e616d657c733a383a224d7568616d6d6164223b656d61696c7c733a32303a22726168656573406f726269746265616d2e6e6574223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313430393438223b637573746f6d65725f69647c4e3b),
('bbbd1e1dc22ecec85c9f5c57a53fcf3cb306c9b8', '202.164.137.235', 1639138926, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133383932363b6964656e746974797c733a32303a22726168656573406f726269746265616d2e6e6574223b757365726e616d657c733a31353a226d7568616d6d616420726168656573223b66697273745f6e616d657c733a383a224d7568616d6d6164223b656d61696c7c733a32303a22726168656573406f726269746265616d2e6e6574223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313338363134223b637573746f6d65725f69647c4e3b),
('bqt9qcm9cijir5j1tquitl07hclm7it3', '::1', 1639135024, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133353032343b6964656e746974797c733a31383a2261646d696e4074656364696172792e636f6d223b757365726e616d657c733a31333a2241646d696e6973747261746f72223b66697273745f6e616d657c733a353a2241646d696e223b656d61696c7c733a31383a2261646d696e4074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313039303539223b637573746f6d65725f69647c4e3b),
('brjncg8ues880m7mfvonm6ctg4ns0m6t', '::1', 1639134706, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133343730363b6964656e746974797c733a31383a2261646d696e4074656364696172792e636f6d223b757365726e616d657c733a31333a2241646d696e6973747261746f72223b66697273745f6e616d657c733a353a2241646d696e223b656d61696c7c733a31383a2261646d696e4074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313039303539223b637573746f6d65725f69647c4e3b),
('c0e5e8197eaed41d0769a9a42101e35b5c4787ea', '202.164.137.235', 1639140250, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393134303235303b6964656e746974797c733a32303a22726168656573406f726269746265616d2e6e6574223b757365726e616d657c733a31353a226d7568616d6d616420726168656573223b66697273745f6e616d657c733a383a224d7568616d6d6164223b656d61696c7c733a32303a22726168656573406f726269746265616d2e6e6574223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313339343434223b637573746f6d65725f69647c4e3b),
('c13e047e7f82c845f6a1efb5ec66a7a96e607ff5', '202.164.137.235', 1639138614, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133383631343b),
('ff234502a89bef1119b796e684471a13c2ef246d', '202.164.137.235', 1639141240, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393134313234303b),
('foe0qjjedapstcpdsokr14bh2mpv335i', '::1', 1639135487, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133353338303b6964656e746974797c733a31383a2261646d696e4074656364696172792e636f6d223b757365726e616d657c733a31333a2241646d696e6973747261746f72223b66697273745f6e616d657c733a353a2241646d696e223b656d61696c7c733a31383a2261646d696e4074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313039303539223b637573746f6d65725f69647c4e3b),
('lv90pjl02fckqgru1klnehihbaq8674s', '::1', 1639135380, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133353338303b6964656e746974797c733a31383a2261646d696e4074656364696172792e636f6d223b757365726e616d657c733a31333a2241646d696e6973747261746f72223b66697273745f6e616d657c733a353a2241646d696e223b656d61696c7c733a31383a2261646d696e4074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313039303539223b637573746f6d65725f69647c4e3b),
('pa495bgg2rmra6ipcnj9q7an08epflcn', '::1', 1639134339, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133343333393b6964656e746974797c733a31383a2261646d696e4074656364696172792e636f6d223b757365726e616d657c733a31333a2241646d696e6973747261746f72223b66697273745f6e616d657c733a353a2241646d696e223b656d61696c7c733a31383a2261646d696e4074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313039303539223b637573746f6d65725f69647c4e3b),
('ss3slcq1un64ltt7qorsj6jhb1i5sshc', '::1', 1639132932, 0x5f5f63695f6c6173745f726567656e65726174657c693a313633393133323933323b6964656e746974797c733a31383a2261646d696e4074656364696172792e636f6d223b757365726e616d657c733a31333a2241646d696e6973747261746f72223b66697273745f6e616d657c733a353a2241646d696e223b656d61696c7c733a31383a2261646d696e4074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231363339313039303539223b637573746f6d65725f69647c4e3b);

-- --------------------------------------------------------

--
-- Table structure for table `sim_settings`
--

CREATE TABLE `sim_settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `invoice_logo` varchar(50) NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `language` varchar(20) NOT NULL,
  `currency_prefix` varchar(3) NOT NULL,
  `default_tax_rate` int(2) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `no_of_rows` int(2) NOT NULL,
  `total_rows` int(2) NOT NULL,
  `dateformat` tinyint(4) NOT NULL,
  `print_payment` tinyint(4) NOT NULL,
  `calendar` tinyint(4) NOT NULL,
  `restrict_sales` tinyint(4) NOT NULL,
  `major` varchar(25) DEFAULT 'Dollars',
  `minor` varchar(25) DEFAULT 'Cents',
  `display_words` tinyint(4) DEFAULT '0',
  `customer_user` tinyint(1) DEFAULT '0',
  `version` varchar(10) DEFAULT '2.2',
  `email_html` tinyint(1) DEFAULT '1',
  `protocol` varchar(20) DEFAULT 'mail',
  `mailpath` varchar(55) DEFAULT '',
  `smtp_host` varchar(55) DEFAULT NULL,
  `smtp_user` varchar(55) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(10) DEFAULT NULL,
  `smtp_crypto` varchar(10) DEFAULT NULL,
  `thousands_sep` varchar(10) DEFAULT ',',
  `decimals_sep` varchar(10) DEFAULT '.',
  `decimals` tinyint(4) DEFAULT '2',
  `default_email` varchar(55) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT NULL,
  `envato_username` varchar(55) DEFAULT NULL,
  `theme` varchar(20) NOT NULL DEFAULT 'default',
  `product_discount` tinyint(1) DEFAULT NULL,
  `barcode_img` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_settings`
--

INSERT INTO `sim_settings` (`setting_id`, `logo`, `invoice_logo`, `site_name`, `language`, `currency_prefix`, `default_tax_rate`, `rows_per_page`, `no_of_rows`, `total_rows`, `dateformat`, `print_payment`, `calendar`, `restrict_sales`, `major`, `minor`, `display_words`, `customer_user`, `version`, `email_html`, `protocol`, `mailpath`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `smtp_crypto`, `thousands_sep`, `decimals_sep`, `decimals`, `default_email`, `purchase_code`, `envato_username`, `theme`, `product_discount`, `barcode_img`) VALUES
(1, 'logo.png', 'logo1.png', 'Nolwa PVT LTD', 'english', 'SR', 0, 10, 2, 3, 5, 0, 0, 0, 'Dollars', 'Cents', 1, 1, '3.7.2', 0, 'mail', '', '', '', '', '', NULL, ',', '.', 2, 'info@nolwa.com', NULL, NULL, 'default', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sim_skrill`
--

CREATE TABLE `sim_skrill` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL DEFAULT 'testaccount2@moneybookers.com',
  `secret_word` varchar(20) NOT NULL DEFAULT 'mbtest',
  `fixed_charges` decimal(25,2) NOT NULL DEFAULT '0.00',
  `extra_charges_my` decimal(25,2) NOT NULL DEFAULT '0.00',
  `extra_charges_other` decimal(25,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_skrill`
--

INSERT INTO `sim_skrill` (`id`, `active`, `account_email`, `secret_word`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'testaccount2@moneybookers.com', 'mbtest', 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `sim_stripe`
--

CREATE TABLE `sim_stripe` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `secret_key` varchar(191) NOT NULL,
  `publishable_key` varchar(191) NOT NULL,
  `fixed_charges` decimal(25,2) NOT NULL DEFAULT '2.00',
  `extra_charges_my` decimal(25,2) NOT NULL DEFAULT '3.90',
  `extra_charges_other` decimal(25,2) NOT NULL DEFAULT '4.40'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_stripe`
--

INSERT INTO `sim_stripe` (`id`, `active`, `secret_key`, `publishable_key`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'sk_test_jHf4cEzAYtgcXvgWPCsQAn50', 'pk_test_beat8SWPORb0OVdF2H77A7uG', 0.00, 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `sim_tax_rates`
--

CREATE TABLE `sim_tax_rates` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `rate` decimal(4,2) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_tax_rates`
--

INSERT INTO `sim_tax_rates` (`id`, `name`, `rate`, `type`) VALUES
(2, 'VAT @15%', 15.00, '1');

-- --------------------------------------------------------

--
-- Table structure for table `sim_users`
--

CREATE TABLE `sim_users` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_users`
--

INSERT INTO `sim_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `customer_id`) VALUES
(2, 0xcaa489eb, 'muhammad rahees', '744bdfe69112e725c62a1037698c21e3f5afb371', NULL, 'rahees@orbitbeam.net', NULL, NULL, NULL, NULL, 1639136426, 1639141603, 1, 'Muhammad', 'Rahees', 'Nolwa Pvt Ltd', '9539966998', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sim_users_groups`
--

CREATE TABLE `sim_users_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sim_users_groups`
--

INSERT INTO `sim_users_groups` (`id`, `user_id`, `group_id`) VALUES
(2, 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sim_calendar`
--
ALTER TABLE `sim_calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_company`
--
ALTER TABLE `sim_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_customers`
--
ALTER TABLE `sim_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_date_format`
--
ALTER TABLE `sim_date_format`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_groups`
--
ALTER TABLE `sim_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_login_attempts`
--
ALTER TABLE `sim_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_notes`
--
ALTER TABLE `sim_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_payment`
--
ALTER TABLE `sim_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_paypal`
--
ALTER TABLE `sim_paypal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_products`
--
ALTER TABLE `sim_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_quotes`
--
ALTER TABLE `sim_quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_quote_items`
--
ALTER TABLE `sim_quote_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_sales`
--
ALTER TABLE `sim_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_sale_items`
--
ALTER TABLE `sim_sale_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_sessions`
--
ALTER TABLE `sim_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `sim_settings`
--
ALTER TABLE `sim_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `sim_skrill`
--
ALTER TABLE `sim_skrill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_stripe`
--
ALTER TABLE `sim_stripe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_tax_rates`
--
ALTER TABLE `sim_tax_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_users`
--
ALTER TABLE `sim_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sim_users_groups`
--
ALTER TABLE `sim_users_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sim_calendar`
--
ALTER TABLE `sim_calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `sim_company`
--
ALTER TABLE `sim_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sim_customers`
--
ALTER TABLE `sim_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sim_date_format`
--
ALTER TABLE `sim_date_format`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sim_groups`
--
ALTER TABLE `sim_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sim_login_attempts`
--
ALTER TABLE `sim_login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sim_notes`
--
ALTER TABLE `sim_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sim_payment`
--
ALTER TABLE `sim_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `sim_products`
--
ALTER TABLE `sim_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sim_quotes`
--
ALTER TABLE `sim_quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sim_quote_items`
--
ALTER TABLE `sim_quote_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sim_sales`
--
ALTER TABLE `sim_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `sim_sale_items`
--
ALTER TABLE `sim_sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `sim_tax_rates`
--
ALTER TABLE `sim_tax_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sim_users`
--
ALTER TABLE `sim_users`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sim_users_groups`
--
ALTER TABLE `sim_users_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
