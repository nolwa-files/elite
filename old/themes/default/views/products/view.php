<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $page_title . ' ' . lang('no') . ' ' . $inv->id; ?></title>
    <link rel="shortcut icon" href="<?= $assets; ?>img/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="<?= $assets; ?>style/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets; ?>style/style.css" rel="stylesheet">
    <style>@page { margin: 0 !important; padding: 0 !important; } html, body { margin: 0 !important; padding: 0 !important; } body, div, span, * { font-family: DejaVu Sans, sans-serif !important; } #wrap { padding: 20px 40px !important; }    .qrimg{
        height : 140px;
    }
    .bg-dark{
        background-color: #ddd;
    }
    .text-left{
        text-align: left !important;
    }
    .text-right{
        text-align: right !important;
    }</style>
</head>

<body>
    <div id="wrap">
        <h4> PROFORMA INVOICE</h4>
         <div class="row">
        <div class="col-xs-8">
                            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                <tr>
                                     <td>Company:</td>
                                     <td><?=$seller[0]->company?> </td>
                                     <td style="direction: rtl;">مرجع أمر العمل :</td>
                                 </tr>
                                 
                                 <tr>
                                     <td>Work Order Reference:</td>
                                     <td><?=$product->reference?> </td>
                                     <td style="direction: rtl;">مرجع أمر العمل :</td>
                                 </tr>
                                
                                 
                             </table>
                         </div>
                            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                 <tr><td >To</td><td></td><td></td><td>ل</td></tr>
                                 <tr>
                                     <td> Name:</td>
                                     <td><?=$product->name ?></td>
                                     <td><?=$product->name_other ?></td>
                                     <td style="direction: rtl;">اسم  :</td>
                                 </tr>
                                 <tr>
                                     <td> Date:</td>
                                     <td><?=  date("d/m/Y", strtotime($product->date)); ?></td>
                                     <td><?=  date("d/m/Y", strtotime($product->date)); ?></td>
                                     <td style="direction: rtl;">تاريخ  :</td>
                                 </tr>
                                 <tr>
                                     <td>VAT No:</td>
                                     <td><?= $product->customer_vat_no ?></td>
                                     <td><?= $product->customer_vat_no ?></td>
                                     <td style="direction: rtl;">ضريبة القيمة المضافة لا: </td>
                                 </tr>
                                 <tr>
                                     <td>Phone:</td>
                                     <td><?= $product->customer_phone ?></td>
                                     <td><?= $product->customer_phone ?></td>
                                     <td style="direction: rtl;">هاتف: </td>
                                 </tr>
                                 <tr>
                                     <td>Email:</td>
                                     <td><?= $product->email ?></td>
                                     <td><?= $product->email ?></td>
                                     <td style="direction: rtl;">البريد الإلكتروني: </td>
                                 </tr>
                             </table>
                         </div>
                         </div>
                         <div class="col-xs-4">
                         <div class="order_barcodes text-center">


                         <?php

                      $cv =$product->price;
                      $app =$product->adv_payment_percentage;
                      $ap =($app*$cv)/100;
                     $tax=$tax_rates[0]->rate;
                     $tap =  ($tax*$ap)/100;
                     $pp= $ap+$tap;
                    
                   $inv_date =date("Y-m-d h:i:s", strtotime($product->date));
                    $vat_total = $pp;
                  
                   $grand_total = $this->sim->formatMoney($cv);
                 
                   $sellers = $seller[0]->name;
                
                   $Vat_No = $seller[0]->cf1;
                 
                   $result = chr(1) . chr( strlen($sellers) ) . $sellers;
                   $result.= chr(2) . chr( strlen($Vat_No) ) . $Vat_No;
                   $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                   $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                   $result.= chr(5) . chr( strlen($vat_total) ) . $vat_total;
                   $newQr = base64_encode($result); 
                  
                   ?>
               
               
               <?= $this->sim->qrcode('url', $newQr, 0); ?>
                    </div>
                         </div>
        </div>
       

        <div class="row">
          
            <p>&nbsp;</p>
          
            <div class="col-xs-12">
            <div class="table-responsive ">
                    <table class="table table-bordered table-hover table-striped print-table order-table">
                      <thead>
                        <tr>
                            <th class="text-center"><span>Item Description</span><br/> </th>
                            <th class="text-center"><span style="direction: rtl;">وصف السلعة </span></th>
                            <th class="text-center"><span>Unit </span><br/> <span style="direction: rtl;">وحدة </span></th>
                            <th class="text-center"><span>Unit price</span><br/> <span style="direction: rtl;">سعر الوحدة</span></th>
                            <th class="text-center"><span>Item Subtotal</span> <br/> <span style="direction: rtl;">المجموع الفرعي للعنصر</span></th>
                        </tr>
                      </thead>
                      <tbody>
                          <td><?=$product->code.' - '.$product->name?></td>
                          <td><?=$product->name_other?></td>
                          <td>1</td>
                          <td><?=$product->price?></td>
                          <td><?= $total = $product->price?></td>
                      </tbody>
                        
                     
                        <tfoot>
                        <tr>
                          <td style="text-align:right; font-weight:bold;"> <span>Total </span>
                             </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;">  <span style="direction: rtl;">مجموع</span>
                             
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $total  ?></td>
                        </tr>
                        <tr>
                          <td style="text-align:right; font-weight:bold;"> <span>Advance Payment Amount </span>
                             </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;">  <span style="direction: rtl;">مبلغ الدفعة المقدمة</span>
                             
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $ap  ?></td>
                        </tr>
                        <tr>
                          <td style="text-align:right; font-weight:bold;"> <span>Tax for advance payment </span>
                             </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;">  <span style="direction: rtl;">ضريبة الدفعة المقدمة</span>
                             
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $tap  ?></td>
                        </tr>
                        <tr>
                          <td style="text-align:right; font-weight:bold;"> <span>Total amount paid as advance </span>
                             </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;">  <span style="direction: rtl;">المبلغ الإجمالي المدفوع مقدما</span>
                             
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $pp  ?></td>
                        </tr>
                       
                        </tfoot>
                    </table>
                </div>
                <center><p>Elite VAT Registration No : <?=$seller[0]->cf1?></p> </center>
                <center><p style="direction: rtl;">رقم تسجيل ضريبة القيمة المضافة للنخبة : <?=$seller[0]->cf1?></p> </center>
            </div>

           
        </div>
    </div>
</body>
</html>
<script>
   $( document ).ready(function() {
   
     window.print();

});

    </script>
