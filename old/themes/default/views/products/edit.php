
<div class="page-head">
    <h2 class="pull-left"><?= $page_title; ?> <span class="page-meta"><?= lang("update_info"); ?></span> </h2>
</div>
<div class="clearfix"></div>
<div class="matter">
    <div class="container">

        <?php
        $attrib = array('class' => 'form-horizontal');
        echo form_open("products/edit?id=".$id);
        ?>
         <div class="form-group">
            <label for="date"><?= lang("date"); ?></label>
            <?php $date = date('Y-m-d H:i'); ?>
            <?= form_input('date', $product->date, 'class="form-control datetime" id="date"');?>
        </div>
 
        <div class="form-group">
            <label for="code"><?= lang("Project Code"); ?></label>
            <div class="controls"> <?= form_input('code', $product->code, 'class="form-control" id="code"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="name"><?= lang("name"); ?></label>
            <div class="controls"> <?= form_input('name', $product->name, 'class="form-control" id="name"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="name"><?= lang("name Other language"); ?></label>
            <div class="controls"> <?= form_input('name_other', $product->name_other, 'class="form-control" id="name_other"');?>
            </div>
        </div>
        
        <div class="form-group">
            <label for="details"><?= lang("details"); ?></label>
            <div class="controls"> <?= form_textarea('details', $product->details, 'class="form-control" id="details" maxlength="255" style="height:60px;"');?>
            </div>
        </div>
       
        <div class="form-group">
            <label for="reference"><?= lang("Work Order Reference"); ?></label>
            <div class="controls"> <?= form_input('reference', $product->reference, 'class="form-control" id="reference"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="price"><?= lang("price"); ?></label>
            <div class="controls"> <?= form_input('price', $product->price, 'class="form-control" id="price"');?>
            </div>
        </div>
        <!-- <?php if ($Settings->default_tax_rate) { ?>
        <div class="form-group">
            <label for="tax_rate"><?= lang("tax_rate"); ?></label>
            <div class="controls">
                <?php
                foreach ($tax_rates as $rate) {
                    $tr[$rate->id] = $rate->name;
                }
                echo form_dropdown('tax_rate', $tr, $product->tax_rate, 'class="form-control" id"tax_rate"'); ?>
            </div>
        </div>

        <div class="form-group">
            <?= lang('tax_method', 'tax_method'); ?>
            <?php $opts = array('exclusive' => lang('exclusive'), 'inclusive' => lang('inclusive')) ?>
            <?= form_dropdown('tax_method', $opts, set_value('tax_method', $product->tax_method), 'class="form-control" id="tax_method"'); ?>
        </div>
        <?php } ?> -->
        <div class="form-group">
            <label for="adv_payment_percentage"><?= lang("Advance Payment Percentage"); ?></label>
            <div class="controls"> <?= form_input('adv_payment_percentage', $product->adv_payment_percentage, 'class="form-control" id="adv_payment_percentage"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="retention_amount"><?= lang("Retention Amount"); ?></label>
            <div class="controls"> <?= form_input('retention_amount', $product->retention_amount, 'class="form-control" id="retention_amount"');?>
            </div>
        </div>
       
        <div class="form-group">
            <label for="customer"><?= lang("Customer"); ?></label>
            <div class="controls"> 
                <select class="form-control" name="customer_id" id="customer_id">
                    <option value="">Select</option>
                    <?php if(!empty($customers)){ foreach($customers as $row){?>
                        <option value="<?=$row->id?>"<?php if($product->customer_id == $row->id){ echo "selected";}?>><?=$row->name?></option>
                        <?php } }?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="reference"><?= lang("Name"); ?></label>
            <div class="controls"> <?= form_input('regad_name', $product->regad_name, 'class="form-control" id="regad_name"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="reference"><?= lang("Number"); ?></label>
            <div class="controls"> <?= form_input('phone', $product->phone, 'class="form-control" id="phone"');?>
            </div>
        </div>
        <div class="form-group">
            <label for="reference"><?= lang("Designation"); ?></label>
            <div class="controls"> <?= form_input('designation', $product->designation, 'class="form-control" id="designation"');?>
            </div>
        </div>
        <div class="form-group">
            <div class="controls"> <?= form_submit('submit', lang("update_product"), 'class="btn btn-primary"');?> </div>
        </div>
        <?= form_close();?>

        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
