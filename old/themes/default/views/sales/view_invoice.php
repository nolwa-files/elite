<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $page_title . ' ' . lang('no') . ' ' . $inv->id; ?></title>
    <link rel="shortcut icon" href="<?= $assets; ?>img/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="<?= $assets; ?>style/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets; ?>style/style.css" rel="stylesheet">
    <style>@page { margin: 0 !important; padding: 0 !important; } html, body { margin: 0 !important; padding: 0 !important; } body, div, span, * { font-family: DejaVu Sans, sans-serif !important; } #wrap { padding: 20px 40px !important; }    .qrimg{
        height : 140px;
    }
    .bg-dark{
        background-color: #ddd;
    }
    .text-left{
        text-align: left !important;
    }
    .text-right{
        text-align: right !important;
    }</style>
</head>

<body>
    <div id="wrap">
        <img src="<?= $this->sim->get_image(base_url('uploads/' . ($biller->logo ? $biller->logo : $Settings->logo))); ?>" alt="<?= $biller->company ? $biller->company : $Settings->site_name ?>" style="margin-top: 20px;" />
        <div class="row">
        <div class="col-xs-8">
                            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                 <tr>
                                     <td>Invoice Number:</td>
                                     <td> <?= $inv->id; ?> </td>
                                     <td class="text-right"> <?= $inv->id; ?> </td>
                                     <td style="direction: rtl;">رقم الفاتورة:</td>
                                 </tr>
                             </table>
                         </div>
                            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                 <tr>
                                     <td>Invoice Issue Date:</td>
                                     <td><?=  date("d/m/Y", strtotime($inv->date)); ?></td>
                                     <td><?=  date("d/m/Y", strtotime($inv->date)); ?></td>
                                     <td style="direction: rtl;">تاريخ إصدار الفاتورة:</td>
                                 </tr>
                                 <tr>
                                     <td>Date of Supply:</td>
                                     <td><?= $delivery->date ?  date("d/m/Y", strtotime($delivery->date)) : date("d/m/Y", strtotime($inv->date)); ?></td>
                                     <td>
                                     <?= $delivery->date ?  date("d/m/Y", strtotime($delivery->date)) : date("d/m/Y", strtotime($inv->date)); ?></td>
                                     <td style="direction: rtl;">تاريخ التوريد:</td>
                                 </tr>
                             </table>
                         </div>
                         </div>
                         <div class="col-xs-4">
                         <div class="order_barcodes text-center">
                         <?php 
                   $inv_date = date("m/d/Y h:i:s A", strtotime($inv->date));
                   $vat_total = $this->sim->formatMoney($inv->order_tax);
                   $grand_total = $this->sim->formatMoney($inv->grand_total);
                   $seller = $biller->company;
                   $Vat_No = $biller->cf1;
                   $result = chr(1) . chr( strlen($seller) ) . $seller;
                   $result.= chr(2) . chr( strlen($Vat_No) ) . $Vat_No;
                   $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                   $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                   $result.= chr(5) . chr( strlen($vat_total) ) . $vat_total;
                   $newQr = base64_encode($result); ?>
                <?= $this->sim->qrcode('url', $newQr, 0); ?>
                    </div>
                         </div>
        </div>
        <div style="clear: both; height: 15px;"></div>

        <div class="row">
        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                    <thead>
                                        <tr class="bg-dark">
                                            <th class="text-left" colspan="2">Seller:</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">تاجر:</th>
                                            <th class="text-left" colspan="2">Buyer:</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">مشتر:</th>
                                        </tr>
                                </thead>
                                <tbody>
                                 <tr >
                                     <td>Name:</td>
                                     <td colspan="2"><?= $biller->company && $biller->company != '-' ? $biller->company : $biller->name; ?></td>
                                     <td style="direction: rtl;">اسم:</td>
                                     <td>Name:</td>
                                     <td  colspan="2"><?= $customer->company && $customer->company != '-' ? $customer->company : $customer->name; ?></td>
                                     <td style="direction: rtl;">اسم:</td>
                                 </tr>
                                 <tr >
                                     <td>Building No:</td>
                                     <td colspan="2"><?= $biller->cf5?></td>
                                     <td style="direction: rtl;">عنوان:</td>
                                     <td>Building No:</td>
                                     <td  colspan="2"><?=$customer->cf5;?></td>
                                     <td style="direction: rtl;">عنوان:</td>
                                 </tr>
                                 <tr >
                                     <td>Street Name :</td>
                                     <td colspan="2"><?= $biller->cf2?></td>
                                     <td style="direction: rtl;">اسم الشارع:</td>
                                     <td>Street Name :</td>
                                     <td  colspan="2"><?=$customer->cf2;?></td>
                                     <td style="direction: rtl;">اسم الشارع:</td>
                                 </tr>
                                 <tr >
                                     <td>District :</td>
                                     <td colspan="2"><?= $biller->state; ?></td>
                                     <td style="direction: rtl;">يصرف:</td>
                                     <td>District :</td>
                                     <td  colspan="2"><?= $customer->state; ?></td>
                                     <td style="direction: rtl;">يصرف:</td>
                                 </tr>
                                 <tr >
                                     <td>City :</td>
                                     <td colspan="2"><?=$biller->city?></td>
                                     <td style="direction: rtl;">مدينة:</td>
                                     <td>City :</td>
                                     <td  colspan="2"><?=$customer->city?></td>
                                     <td style="direction: rtl;">مدينة:</td>
                                 </tr>
                                 <tr >
                                     <td>Country :</td>
                                     <td colspan="2"><?=$biller->country?></td>
                                     <td style="direction: rtl;">دولة:</td>
                                     <td>Country :</td>
                                     <td  colspan="2"><?=$customer->country?></td>
                                     <td style="direction: rtl;">دولة:</td>
                                 </tr>
                                 <tr >
                                     <td>Postal Code :</td>
                                     <td colspan="2"><?= $biller->postal_code?></td>
                                     <td style="direction: rtl;">رمز بريدي:</td>
                                     <td>Postal Code :</td>
                                     <td  colspan="2"><?= $customer->postal_code?></td>
                                     <td style="direction: rtl;">رمز بريدي:</td>
                                 </tr>
                                 <tr >
                                     <td>Additional No :</td>
                                     <td colspan="2"><?=$biller->cf3;?></td>
                                     <td style="direction: rtl;">رقم إضافي:</td>
                                     <td>Additional No :</td>
                                     <td  colspan="2"><?=$customer->cf3;?></td>
                                     <td style="direction: rtl;">رقم إضافي:</td>
                                 </tr>
                                 <tr >
                                     <td>VAT Number :</td>
                                     <td colspan="2"><?= $biller->cf1?></td>
                                     <td style="direction: rtl;">ظريبه الشراء:</td>
                                     <td>VAT Number :</td>
                                     <td  colspan="2"></td>
                                     <td style="direction: rtl;">ظريبه الشراء:</td>
                                 </tr>
                                 <tr >
                                     <td>Other Seller ID :</td>
                                     <td colspan="2"><?=$biller->cf4;?></td>
                                     <td style="direction: rtl;">معرف البائع الآخر:</td>
                                     <td>Other Seller ID :</td>
                                     <td  colspan="2"><?=$customer->cf4;?></td>
                                     <td style="direction: rtl;">معرف البائع الآخر:</td>
                                 </tr>
                                </tbody>
                             </table>
                         </div>  
                    </div>
            <p>&nbsp;</p>
            <div style="clear: both; height: 15px;"></div>
            <div class="col-xs-12">
            <div class="table-responsive ">
                    <table class="table table-bordered table-hover table-striped print-table order-table">
                        <thead>
                        <tr>
                            <th colspan="4" class="text-left"><?= lang('Line_Items'); ?></th> <br/>
                            <th colspan="4" class="text-right" style="direction: rtl;">قائمة العناصر</th>
                        </tr>
                        </thead>
                        <thead>

                        <tr>
                            <th class="text-center"><span>Nature of goods
or services</span> <br/><span style="direction: rtl;">طبيعة السلع
أو الخدمات</span></th>
                            <th class="text-center"><span>Unit price</span><br/> <span style="direction: rtl;">سعر الوحدة</span></th>
                            <th class="text-center"><span>Quantity</span> <br/> <span style="direction: rtl;">كمية</span></th>
                            <?php
                            ?>
                            <th class="text-center"><span>Taxable Amount</span> <br/> <span style="direction: rtl;">المبلغ الخاضع للضريبة</span></th>
                            <th style="padding-right:20px; text-align:center; vertical-align:middle;">Discount<br/> <span style="direction: rtl;">خصم</span> </th>
                            <th class="text-center"><span>Tax Rate</span> <br/> <span style="direction: rtl;">معدل الضريبة</span></th>
                            <th class="text-center"><span>Tax Amount</span> <br/> <span style="direction: rtl;">قيمة الضريبة</span></th>
                            <th class="text-center"><span>Item Subtotal
(Including VAT)</span> <br/> <span style="direction: rtl;">المجموع الفرعي للعنصر
(بما في ذلك ضريبة القيمة المضافة)</span></th>
                            
                        </tr>

                        </thead>

                        <tbody>

                        <?php 
                        foreach ($rows as $row):
                            ?>
                            <tr>
                                <td style="text-align:center; vertical-align:middle;"><?= $row->details ? '<strong>' . $row->product_name . '</strong><br>' . $row->details : $row->product_name; ?></td>
                                <td style="vertical-align:middle;">
                                  <?= $this->sim->formatMoney($row->real_unit_price); ?>
                                </td>
                                <td style="text-align:center; vertical-align:middle;"><?= $row->quantity; ?></td>
                                <td style="text-align:center; padding-right:10px;">
                                    <?php $taxableAmount = ($row->discount_amt > 0 ? $row->real_unit_price - $row->discount : $row->real_unit_price) * $row->quantity;?>
                                    <?= $this->sim->formatMoney($taxableAmount); ?>
                                </td>
                                <td style="text-align:center; padding-right:10px;">
                                <?= $row->discount_amt > 0 ? '<small>(' . $row->discount . ')</small>' : ''; ?>
                                        <?= $this->sim->formatMoney($row->discount_amt); ?>
                                </td>
                                <td style="text-align:center;  padding-right:10px;"> 15 %</td>
                                <td style="text-align:center;  padding-right:10px;"><?php $totalVat =($taxableAmount) * (15) / 100; ?>
                                <?= $this->sim->formatMoney($totalVat); ?></td>
                                <td style="text-align:center;  padding-right:10px;">
                                <!-- <?= $this->sim->formatMoney($row->subtotal); ?> -->
                                 <?= $this->sim->formatMoney($taxableAmount + $totalVat); ?> 
                            </td>
                            </tr>
                            <?php
                            $r++;
                        endforeach;
                        ?>
                        </tbody>
                        
                        <tfoot>
                        <tr>
                        <td> </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span>Total (Excluding VAT)</span>
                                (<?= $Settings->currency_prefix; ?>)
                                
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;">  <span style="direction: rtl;">الإجمالي (باستثناء ضريبة القيمة المضافة)</span>
                                (<?= $Settings->currency_prefix; ?>)
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sim->formatMoney($inv->total); ?></td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span>Discount</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;">  <span style="direction: rtl;">خصم</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sim->formatMoney($inv->order_discount); ?></td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span>Total Taxable Amount (Excluding 
VAT)(<?= $Settings->currency_prefix; ?>)</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي المبلغ الخاضع للضريبة (باستثناء ضريبة القيمة المضافة) (<?= $Settings->currency_prefix; ?>)</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sim->formatMoney(($inv->total - $inv->order_discount)); ?></td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span>Total VAT</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي ضريبة القيمة المضافة</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sim->formatMoney($inv->order_tax); ?></td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span>Total Amount Due</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي المبلغ المستحق</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sim->formatMoney($inv->grand_total); ?></td>
                        </tr>

                        </tfoot>
                    </table>
                </div>
            </div>

            <div style="clear: both;"></div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                        if ($inv->note && $inv->note != '<br>' && $inv->note != ' ' && $inv->note != '<p></p>') {
                            ?>
                            <p>&nbsp;</p>
                            <div class="well well-sm">
                                <p class="lead"><?= lang('note'); ?>:</p>
                                <p><?= $inv->note; ?></p>
                            </div>
                            <?php
                        }
                        ?>
                    </div>

                    <div style="clear: both;"></div>
                    <div class="col-xs-4 pull-left">
                        <?php
                        if ($biller->ss_image) {
                            ?>
                            <img src="<?= $this->sim->get_image(base_url('uploads/' . $biller->ss_image)); ?>" alt="" />
                            <?php
                        } else {
                            ?>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <?php
                        }
                        ?>
                        <p style="border-bottom: 1px solid #999;">&nbsp;</p>
                        <p><?= lang('signature') . ' &amp; ' . lang('stamp'); ; ?></p>
                    </div>

                    <div class="col-xs-4 pull-right">
                        <p>&nbsp;</p>
                        <p>
                            <?= lang('buyer'); ?>:
                            <?php
                            if ($customer->company != '-') {
                                echo $customer->company;
                            } else {
                                echo $customer->name;
                            }
                            ?>
                        </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p style="border-bottom: 1px solid #999;">&nbsp;</p>
                        <p><?= lang('signature') . ' &amp; ' . lang('stamp'); ; ?></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 15px;">
            <?php
            if (isset($client) && !empty($client)) {
                echo '<a class="btn btn-primary btn-block" href="' . site_url('clients/pdf?id=' . $inv->id) . '">' . lang('download_pdf') . '</a>';
            }
            ?>
            </div>
                <?php
                if ($Settings->print_payment) {
                    if (!empty($payment)) {
                        ?>
                    <div class="clearfix"></div>
                    <div style="page-break-after: always;"></div>
                    <div class="col-xs-12" style="margin-top: 15px;">
                        <h4><?= lang('payment_details'); ?> (<?= $page_title . ' ' . lang('no') . ' ' . $inv->id; ?>)</h4>
                        <table class="table table-bordered table-condensed table-hover table-striped" style="margin-bottom: 5px;">

                            <thead>
                                <tr>
                                    <th><?= lang('date'); ?></th>
                                    <th><?= lang('amount'); ?></th>
                                    <th><?= lang('note'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($payment as $p) {
                                    ?>
                                    <tr>
                                        <td><?= $this->sim->hrsd($p->date); ?></td>
                                        <td><?= $this->sim->formatMoney($p->amount); ?></td>
                                        <td><?= $p->note; ?></td>
                                    </tr>
                                    <?php
                                } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    }
                }
            ?>
        </div>
    </div>
</div>
</body>
</html>
