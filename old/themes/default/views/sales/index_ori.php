<script>
    $(document).ready(function() {
        $('#fileData').dataTable( {
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "aaSorting": [[ 0, "asc" ]],
            "iDisplayLength": 10,
            "oTableTools": {
                "sSwfPath": "<?= $assets; ?>media/swf/copy_csv_xls_pdf.swf",
                "aButtons": [ "csv", "xls", { "sExtends": "pdf", "sPdfOrientation": "landscape", "sPdfMessage": "" }, "print" ]
            },
            "aoColumns": [ null, null, null, null, null, { "bSortable": false } ]
        });
    });
</script>

<div class="page-head">
    <h2 class="pull-left"><?= $page_title; ?> <span class="page-meta"><?= lang("list_results_x"); ?></span> </h2>
</div>
<div class="clearfix"></div>
<div class="matter">
    <div class="container">
        <table id="fileData" class="table table-bordered table-hover table-striped" style="margin-bottom: 5px;">
                     <thead>
                <tr class="active">
                    <th><?=lang("id");?></th>
                    <th ><?=lang("date");?></th>
                    <th ><?=lang("Project");?></th>
                    
                    <th ><?=lang("Reference No");?></th>
                     <th ><?=lang("customer");?></th>
                   <th style="width:100px;"><?=lang("actions");?></th>
                </tr>
                </thead>
                <tbody>
             
              
                <?php 
                 
                
                $i=1;foreach($sales as $sales){ 
            
                    
                    
                    ?>
                    <tr>
                        <td><?=$i?></td>
                        <td><?=$sales->date?></td>
                        <td><?=$sales->project_name?></td>
                        <td><?=$sales->reference_no?></td>
                        <td><?=$sales->company?></td>
                        <td>
                        <!-- <a class="tip btn btn-primary btn-xs"  href="<?=base_url()?>sales/edit_sale/<?=$sales->id?>"> <i class="fa fa-edit"></i> </a>    -->
                        <a  class="tip btn btn-info btn-xs" href="<?=base_url()?>sales/view_invoice_new/<?=$sales->id?>"><i class="fa fa-eye"></i> </a>
                        <a class="tip btn btn-danger btn-xs" title="delete_sale" href="<?=base_url()?>sales/delete_sale/<?=$sales->id?>" onClick="return confirm(\''. lang('alert_x_user') .'\');">
                                <i class="fa fa-trash-o"></i>
                            </a>
                    </td>
                       


                    </tr>

                    <?php $i++; } ?>
</tbody>
               
               
            </table>
        </div>


        