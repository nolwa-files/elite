<style>
    .qrimg{
        height : 140px;
    }
    .bg-dark{
        background-color: #ddd;
    }
    .text-left{
        text-align: left !important;
    }
    .text-right{
        text-align: right !important;
    }
</style>
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <div class="modal-body">
    <img src="<?= $assets; ?>img/<?= $inv->status; ?>.png" alt="<?= $inv->status; ?>" style="float: right; position: absolute; top:0; right: 0;"/>
  
    <div id="wrap">

        <img src="<?= base_url(); ?>uploads/<?= $biller->logo ? $biller->logo : $Settings->logo; ?>" alt="<?= $biller->company ? $biller->company : $Settings->site_name ?>" />
        <div class="row">
        <div class="well well-sm">
                         <div class="col-xs-8 padding010">
                         <div class="clearfix"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                 <tr>
                                     <td>Invoice Number:</td>
                                     <td> <?= $inv->id; ?> </td>
                                     <td class="text-right"> <?= $inv->id; ?> </td>
                                     <td style="direction: rtl;">رقم الفاتورة:</td>
                                 </tr>
                             </table>
                         </div>
                            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                 <tr>
                                     <td>Invoice Date:</td>
                                     <td><?=  date("d/m/Y", strtotime($inv->date)); ?></td>
                                     <td><?=  date("d/m/Y", strtotime($inv->date)); ?></td>
                                     <td style="direction: rtl;">تاريخ:</td>
                                 </tr>
                                 <tr>
                                     <td>Work Order Reference:</td>
                                     <td><?=  $inv->reference_no; ?></td>
                                     <td><?=  $inv->reference_no; ?></td>
                                     <td style="direction: rtl;">مرجع أمر العمل:</td>
                                 </tr>
                                 
                             </table>
                         </div>
                         </div>
                         <div class="col-xs-4 padding010">
                         <div class="clearfix"></div>
                         <div class="order_barcodes text-right">
                         <?php 
                   $inv_date = date("m/d/Y h:i:s A", strtotime($inv->date));
                   $vat_total = $this->sim->formatMoney($inv->order_tax);
                   $grand_total = $this->sim->formatMoney($inv->grand_total);
                   $seller = $biller->company;
                   $Vat_No = $biller->cf1;
                   $result = chr(1) . chr( strlen($seller) ) . $seller;
                   $result.= chr(2) . chr( strlen($Vat_No) ) . $Vat_No;
                   $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                   $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                   $result.= chr(5) . chr( strlen($vat_total) ) . $vat_total;
                   $newQr = base64_encode($result); ?>
                <?= $this->sim->qrcode('url', $newQr, 0); ?>
                    </div>
                         </div>
                         <div class="col-xs-12 padding010">
                         <div class="clearfix"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                    <thead>
                                        <tr class="bg-dark">
                                            <th class="text-left" colspan="2">Seller:</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">تاجر:</th>
                                            <th class="text-left" colspan="2">Buyer:</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">مشتر:</th>
                                        </tr>
                                </thead>
                                <tbody>
                                 <tr >
                                     <td>Name:</td>
                                     <td colspan="2"><?= $biller->company && $biller->company != '-' ? $biller->company : $biller->name; ?></td>
                                     <td style="direction: rtl;">اسم:</td>
                                     <td>Name:</td>
                                     <td  colspan="2"><?= $customer->company && $customer->company != '-' ? $customer->company : $customer->name; ?></td>
                                     <td style="direction: rtl;">اسم:</td>
                                 </tr>
                                
                                
                                
                                 
                                 <tr >
                                     <td>Phone :</td>
                                     <td colspan="2"><?=$biller->phone?></td>
                                     <td style="direction: rtl;">هاتف:</td>
                                     <td>phone :</td>
                                     <td  colspan="2"><?=$customer->phone?></td>
                                     <td style="direction: rtl;">هاتف:</td>
                                 </tr>
                                 <tr >
                                     <td>Email :</td>
                                     <td colspan="2"><?= $biller->email; ?></td>
                                     <td style="direction: rtl;">البريد الإلكتروني:</td>
                                     <td>Email :</td>
                                     <td  colspan="2"><?= $customer->email; ?></td>
                                     <td style="direction: rtl;">البريد الإلكتروني:</td>
                                 </tr>
                                 <!-- <tr >
                                     <td>Postal Code :</td>
                                     <td colspan="2"><?= $biller->postal_code?></td>
                                     <td style="direction: rtl;">رمز بريدي:</td>
                                     <td>Postal Code :</td>
                                     <td  colspan="2"><?= $customer->postal_code?></td>
                                     <td style="direction: rtl;">رمز بريدي:</td>
                                 </tr> -->
                                
                                 <tr >
                                     <td>VAT Number :</td>
                                     <td colspan="2"><?= $biller->cf1?></td>
                                     <td style="direction: rtl;">ظريبه الشراء:</td>
                                     <td>VAT Number :</td>
                                     <td  colspan="2"><?=$customer->cf1?></td>
                                     <td style="direction: rtl;">ظريبه الشراء:</td>
                                 </tr>
                                 <tr >
                                     <td>Address :</td>
                                     <td colspan="2"><?= $biller->address?></td>
                                     <td style="direction: rtl;">تبوك :</td>
                                     <td>Address :</td>
                                     <td  colspan="2"><?=$customer->address?></td>
                                     <td style="direction: rtl;">تبوك :</td>
                                 </tr>
                                
                                </tbody>
                             </table>
                         </div>  
                    </div>
                    <div class="clearfix"></div>
                </div>
        </div>
        <div style="clear: both; height: 15px;"></div>

        <div class="row">
            <p>&nbsp;</p>
            <div style="clear: both; height: 15px;"></div>
            <div class="col-xs-12">
            <div class="table-responsive ">
                    <table class="table table-bordered table-hover table-striped print-table order-table">
                        <thead>
                        <tr>
                            <th colspan="3" class="text-left"><?= lang('Line_Items'); ?></th> <br/>
                            <th colspan="3" class="text-right" style="direction: rtl;">قائمة العناصر</th>
                        </tr>
                        </thead>
                        <thead>

                        <tr>
                            <th class="text-center"><span>Product</span> <br/><span style="direction: rtl;"> منتج </span></th>
                            <th class="text-center"><span>Unit price</span><br/> <span style="direction: rtl;">سعر الوحدة</span></th>
                            <th class="text-center"><span>Quantity</span> <br/> <span style="direction: rtl;">كمية</span></th>
                            <?php
                            ?>
                            <!-- <th class="text-center"><span>Taxable Amount</span> <br/> <span style="direction: rtl;">المبلغ الخاضع للضريبة</span></th> -->
                            <th class="text-center"><span>Tax Rate</span> <br/> <span style="direction: rtl;">معدل الضريبة</span></th>
                            <th class="text-center"><span>Tax Amount</span> <br/> <span style="direction: rtl;">قيمة الضريبة</span></th>
                            <th class="text-center"><span>Item Subtotal
                (Including VAT)</span> <br/> <span style="direction: rtl;">المجموع الفرعي للعنصر
                (بما في ذلك ضريبة القيمة المضافة)</span></th>
                            
                        </tr>

                        </thead>

                        <tbody>

                        <?php 
                        $cv = $inv->contractual_value;
                        $adv_paid = $inv->adv_pay_per;
                        $adv_paid_amount = ($cv* $adv_paid)/100;   
                        foreach ($rows as $row):
                            ?>
                            <tr>
                                <td style="text-align:center; vertical-align:middle;"><?= $row->details ? '<strong>' . $row->product_name . '</strong><br>' . $row->details : $row->product_name; ?></td>
                                <td style="vertical-align:middle;">
                                  <?= $this->sim->formatMoney($row->real_unit_price); ?>
                                </td>
                                <td style="text-align:center; vertical-align:middle;"><?= $row->quantity; ?></td>
                                <!-- <td style="text-align:center; padding-right:10px;">
                                    <?php $taxableAmount = ($row->discount_amt > 0 ? $row->real_unit_price - $row->discount : $row->real_unit_price) * $row->quantity;?>
                                    <?= $this->sim->formatMoney($taxableAmount); ?>
                                </td> -->
                                
                                <td style="text-align:center;  padding-right:10px;"> 15 %</td>
                                <td style="text-align:center;  padding-right:10px;"><?php $totalVat =($taxableAmount) * (15) / 100; ?>
                                <?= $this->sim->formatMoney($totalVat); ?></td>
                                <td style="text-align:center;  padding-right:10px;">
                                <!-- <?= $this->sim->formatMoney($row->subtotal); ?> -->
                                 <?= $this->sim->formatMoney($taxableAmount + $totalVat); ?> 
                            </td>
                            </tr>
                            <?php
                            $r++;
                        endforeach;
                        ?>
                        </tbody>
                        
                        <tfoot>
                        <tr>
                            <td colspan="2"
                                style="text-align:right; font-weight:bold;"> <span>Total (Excluding VAT)</span>
                                (<?= $Settings->currency_prefix; ?>)
                                
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;">  <span style="direction: rtl;">الإجمالي (باستثناء ضريبة القيمة المضافة)</span>
                                (<?= $Settings->currency_prefix; ?>)
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sim->formatMoney($inv->total); ?></td>
                        </tr>
                        
                        <tr>
                            <td colspan="2"
                                style="text-align:right; font-weight:bold;"> <span>Total Taxable Amount (Excluding 
VAT)(<?= $Settings->currency_prefix; ?>)</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي المبلغ الخاضع للضريبة (باستثناء ضريبة القيمة المضافة) (<?= $Settings->currency_prefix; ?>)</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sim->formatMoney(($inv->total - $inv->order_discount)); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"
                                style="text-align:right; font-weight:bold;"> <span>Total VAT</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي ضريبة القيمة المضافة</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sim->formatMoney($inv->order_tax); ?></td>
                        </tr>
                        <tr>
                             <td colspan="2"
                                style="text-align:right; font-weight:bold;"> <span>Total Amount Due</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي المبلغ المستحق</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sim->formatMoney($inv->grand_total); ?></td>
                        </tr>
                        <tr>
                             <td colspan="2"
                                style="text-align:right; font-weight:bold;"> <span>Total Amount Due</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي المبلغ المستحق</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?= $this->sim->formatMoney($inv->grand_total); ?></td>
                        </tr>
                        <tr>
                             <td colspan="2"
                                style="text-align:right; font-weight:bold;"> <span>Total Work Done</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">إجمالي العمل المنجز</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"></td>
                        </tr>
                        <tr>
                             <td colspan="2"
                                style="text-align:right; font-weight:bold;"> <span>Previous Paid</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">المدفوعة السابقة</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"></td>
                        </tr>
                        <tr>
                             <td colspan="2"
                                style="text-align:right; font-weight:bold;"> <span>Advance Paid</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">الدفع المسبق</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?=$adv_paid_amount?></td>
                        </tr>
                        <tr>
                             <td colspan="2"
                                style="text-align:right; font-weight:bold;"> <span>Retention</span>                               
                            </td>
                            <td colspan="3"
                                style="text-align:right; font-weight:bold;"> <span style="direction: rtl;">احتفاظ</span>
                            </td>
                            <td style="text-align:center; padding-right:10px; font-weight:bold;"><?=$inv->retention_amt?></td>
                        </tr>

                        </tfoot>
                    </table>
                </div>
                <div style="clear: both;"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                        if ($inv->note && $inv->note != '<br>' && $inv->note != ' ' && $inv->note != '<p></p>') {
                            ?>
                            <p>&nbsp;</p>
                            <div class="well well-sm">
                                <p class="lead"><?= lang('note'); ?>:</p>
                                <p><?= $inv->note; ?></p>
                            </div>
                            <?php
                        }
                        ?>
                    </div>

                    <div style="clear: both;"></div>
                    <div class="col-xs-4 pull-left">
                        <?php
                        if ($biller->ss_image) {
                            ?>
                            <img src="<?= base_url('uploads/' . $biller->ss_image); ?>" alt="" />
                            <?php
                        } else {
                            ?>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <?php
                        }
                        ?>
                        <p style="border-bottom: 1px solid #999;">&nbsp;</p>
                        <p><?= lang('signature') . ' &amp; ' . lang('stamp'); ; ?></p>
                    </div>

                    <div class="col-xs-4 pull-right">
                        <p>&nbsp;</p>
                        <p>
                            <?= lang('buyer'); ?>:
                            <?php
                            if ($customer->company != '-') {
                                echo $customer->company;
                            } else {
                                echo $customer->name;
                            }
                            ?>
                        </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p style="border-bottom: 1px solid #999;">&nbsp;</p>
                        <p><?= lang('signature') . ' &amp; ' . lang('stamp'); ; ?></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top: 15px;">
            <?php
            if ((isset($client) && !empty($client)) || (isset($spay) && !empty($spay))) {
                ?>
                <div class="no-print">
                    <?php $grand_total = ($inv->grand_total - $paid);
                if ($inv->status != 'paid') { ?>
                    <div class="well well-sm">
                        <div id="payment_buttons" class="text-center margin010" style="display:flex;align-items: center;justify-content:space-between;">
                        <?php
                        if ($stripe->active == 1 && $grand_total != '0.00') {
                            $stripe_fee = 0;
                            if (trim(strtolower($customer->country)) == $biller->country) {
                                $stripe_fee = $stripe->fixed_charges + ($grand_total * $stripe->extra_charges_my / 100);
                            } else {
                                $stripe_fee = $stripe->fixed_charges + ($grand_total * $stripe->extra_charges_other / 100);
                            } ?>
                            <!-- <div style="max-width:250px;margin-right:20px;display:inline-block;float:left;">
                                <a class="btn btn-primary btn-block no-print" style="border-radius:5px!important;padding:1rem 2rem;font-size:2rem!important;font-weight:bold;" href="<?= site_url('payments/stripe/' . $inv->id) . '/' . base64_encode($inv->id); ?>"><?=lang('pay_with_cc'); ?></a>
                            </div> -->
                            <?php
                        }
                        ?>
                            <?php
                            if (!isset($spay) && $paypal->active == 1 && $grand_total != '0.00') {
                                $paypal_fee = 0;
                                if (trim(strtolower($customer->country)) == $biller->country) {
                                    $paypal_fee = $paypal->fixed_charges + ($grand_total * $paypal->extra_charges_my / 100);
                                } else {
                                    $paypal_fee = $paypal->fixed_charges + ($grand_total * $paypal->extra_charges_other / 100);
                                } ?>

                                <div style="width:150px;margin-right:10px;display:inline-flex;align-items:center;">
                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                        <input type="hidden" name="cmd" value="_xclick">
                                        <input type="hidden" name="business" value="<?=  $paypal->account_email; ?>">
                                        <input type="hidden" name="item_name" value="<?=  $inv->reference_no; ?>">
                                        <input type="hidden" name="item_number" value="<?=  $inv->id; ?>">
                                        <input type="hidden" name="image_url" value="<?=  base_url() . 'uploads/logos/' . ($biller->logo ? $biller->logo : $Settings->logo); ?>">
                                        <input type="hidden" name="amount" value="<?=  $grand_total + $paypal_fee; ?>">
                                        <input type="hidden" name="no_shipping" value="1">
                                        <input type="hidden" name="no_note" value="1">
                                        <input type="hidden" name="currency_code" value="<?= $Settings->currency_prefix; ?>">
                                        <input type="hidden" name="bn" value="FC-BuyNow">
                                        <input type="image"  id="no-pdf" src="<?= base_url('uploads/btn-paypal.png'); ?>" name="submit" alt="Pay by PayPal" style="border:0 !important;">
                                        <input type="hidden" name="rm" value="2">
                                        <input type="hidden" name="return" value="<?=  site_url('clients/view_invoice?id=' . $inv->id); ?>">
                                        <input type="hidden" name="cancel_return" value="<?=  site_url('clients/view_invoice?id=' . $inv->id); ?>">
                                        <input type="hidden" name="notify_url" value="<?=  site_url('payments/paypalipn'); ?>" />
                                        <input type="hidden" name="custom" value="<?=  $inv->reference_no . '__' . $grand_total . '__' . $paypal_fee; ?>">
                                    </form>
                                </div>
                                <?php
                            }
                            if (!isset($spay) && $skrill->active == 1 && $grand_total != '0.00') {
                                $skrill_fee = 0;
                                if (trim(strtolower($customer->country)) == $biller->country) {
                                    $skrill_fee = $skrill->fixed_charges + ($grand_total * $skrill->extra_charges_my / 100);
                                } else {
                                    $skrill_fee = $skrill->fixed_charges + ($grand_total * $skrill->extra_charges_other / 100);
                                } ?>
                                <div style="width:170px;margin-left:10px;display:inline-block;">
                                    <form action="https://www.moneybookers.com/app/payment.pl" method="post">
                                        <input type="hidden" name="pay_to_email" value="<?= $skrill->account_email; ?>">
                                        <input type="hidden" name="status_url" value="<?= site_url('payments/skrillipn'); ?>">
                                        <input type="hidden" name="cancel_url" value="<?= site_url('clients/view_invoice?id=' . $inv->id); ?>">
                                        <input type="hidden" name="return_url" value="<?= site_url('clients/view_invoice?id=' . $inv->id); ?>">
                                        <input type="hidden" name="language" value="EN">
                                        <input type="hidden" name="ondemand_note" value="<?=  $inv->reference_no; ?>">
                                        <input type="hidden" name="merchant_fields" value="item_name,item_number">
                                        <input type="hidden" name="item_name" value="<?= $inv->reference_no; ?>">
                                        <input type="hidden" name="item_number" value="<?= $inv->id; ?>">
                                        <input type="hidden" name="amount" value="<?= $grand_total + $skrill_fee; ?>">
                                        <input type="hidden" name="currency" value="<?= $Settings->currency_prefix; ?>">
                                        <input type="hidden" name="detail1_description" value="<?=  $inv->reference_no; ?>">
                                        <input type="hidden" name="detail1_text" value="Payment for the sale invoice <?= $inv->reference_no . ': ' . $grand_total . '(+ fee: ' . $skrill_fee . ') = ' . ($grand_total + $skrill_fee); ?>">
                                        <input type="hidden" name="logo_url" value="<?= base_url() . 'uploads/logos/' . ($biller->logo ? $biller->logo : $Settings->logo); ?>">
                                        <input type="image" id="no-pdf" src="<?= base_url('uploads/btn-skrill.png'); ?>" name="submit" alt="Pay by Skrill" style="border:0 !important;">
                                    </form>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php
                echo '<a class="btn btn-primary btn-block no-print" href="' . site_url('clients/pdf?id=' . $inv->id) . '">' . lang('download_pdf') . '</a>';
                echo '<a class="btn btn-primary btn-block no-print printDivs" href="javascript:void(0)" >' . lang('print') . '</a>';
            }
            ?>
            
                <?php
                if ($Settings->print_payment) {
                    if (!empty($payment)) {
                        ?>
                    <div class="page-break"></div>
                    <h4><?= lang('payment_details'); ?> (<?= $page_title . ' ' . lang('no') . ' ' . $inv->id; ?>)</h4>
                    <table class="table table-bordered table-condensed table-hover table-striped" style="margin-bottom: 5px;">

                        <thead>
                            <tr>
                                <th><?= lang('date'); ?></th>
                                <th><?= lang('amount'); ?></th>
                                <th><?= lang('note'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($payment as $p) {
                                ?>
                                <tr>
                                    <td><?= $this->sim->hrsd($p->date); ?></td>
                                    <td><?= $this->sim->formatMoney($p->amount); ?></td>
                                    <td><?= $p->note; ?></td>
                                </tr>
                                <?php
                            } ?>
                        </tbody>
                    </table>
                    <?php
                    }
                }
            ?>
        </div>
    </div>
</div>
</div>
</div>
</div>
<script>
   $( document ).ready(function() {
   
    $(".printDivs").click(function () {
    //     var printContents = document.getElementById('printDiv').innerHTML;
    //  var originalContents = document.body.innerHTML;

    //  document.body.innerHTML = printContents;

     window.print();

    //  document.body.innerHTML = originalContents;
    //   return true;
});
})
    </script>